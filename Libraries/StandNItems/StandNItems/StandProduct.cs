﻿using System;
using System.Collections.Generic;
using System.Text;
// using System.Globalization; // In case of wanting to make use of a specific currency for the string formatter.

namespace StandNItems
{
    public abstract class StandProduct : Product
    {
        // Fields
        private decimal price;

        // Product ID comes from the database, for both product types
        // the database is queried to assign this number to the corresponding product.
        
        // Properties
        public decimal Price
        {
            get { return price; }
            set
            {
                if (value >= 0)
                {
                    price = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Price", value, "Price has to be greater than or equal to 0.");
                }
            }
        }
        // Constructors
        public StandProduct(int id, string name, decimal price) : base(id, name)
        {
            Price = price;
        }

        // Methods
        public override string ToString()
        {
            return $"{base.ToString()}, {Price.ToString("C")}";
        }
    }
}