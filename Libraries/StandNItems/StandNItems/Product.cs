﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StandNItems
{
    public class Product : IComparable<Product>
    {
        public int Id { get; private set; }
        public string Name { get; set; }

        public Product(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public override string ToString()
        {
            return $"{Id}, {Name}";
        }

        public int CompareTo(Product that)
        {
            if (Id < that.Id)
            {
                return -1;
            }
            return 1;
        }
    }
}
