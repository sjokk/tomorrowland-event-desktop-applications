﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StandNItems
{
    public class ConsumableStand : Stand
    {
        // Properties
        public int StandId { get; private set; }
        public string Name { get; set; }

        // Constructors
        public ConsumableStand(int standId, string name) : base()
        {
            StandId = standId;
            Name = name;
        }

        /// <summary>
        /// Here objects of Receipt get thrown around.
        /// </summary>
        /// <param name="receipt"></param>
        public void DeductInventory(Receipt receipt)
        {
            ConsumableProduct cp;
            foreach (ReceiptLine rl in receipt.GetReceiptLines())
            {
                cp = (ConsumableProduct)GetProduct(rl.ProductId);

                cp.NumberInStock -= rl.SalesQuantity;
            }
        }
    }
}