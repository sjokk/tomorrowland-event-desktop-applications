﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StandNItems
{
    public class ConsumableProduct : StandProduct
    {
        // Fields
        private int numberInStock;

        // Properties
        public int NumberInStock
        {
            get { return numberInStock; }
            set
            {
                if (value >= 0)
                {
                    numberInStock = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("NumberInStock", "NumberInStock can not be negative.");
                }
            }
        }
        // Constructors
        public ConsumableProduct(int id, string name, decimal price, int numberInStock) : base(id, name, price)
        {
            NumberInStock = numberInStock;
        }

        // Methods
        public override string ToString()
        {
            return $"{base.ToString()}, {NumberInStock}";
        }
    }
}
