﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StandNItems
{
    public class EventStandManagement
    {
        // Fields
        // Every stand regardless of the type will hold a list of products.
        protected List<Product> productList;

        // Constructors
        public EventStandManagement()
        {
            // Initialize a new list to hold products.
            productList = new List<Product>();
            // Indicate that newly created stand has not yet been modified.
        }
        
        // General Methods
        // Method to search and obtain a product by its product id.
        public Product GetProduct(int id)
        {
            // For every product in the list of products.
            foreach (Product currentP in productList)
            {
                // If the current product's corresponds to the entered id, we return that product. 
                if (currentP.Id == id)
                {
                    return currentP;
                }
            }
            // If the loop is exhausted, no product with id was found and null is returned.
            return null;
        }

        public bool RemoveProduct(int id)
        {
            // Obtain product by id, if it exitst, remove from product list.
            Product p = GetProduct(id);
            if (p != null)
            {
                productList.Remove(p);
                return true;
            }
            return false;
        }

        public List<Product> GetProducts()
        {
            // Returns a copy of the original list.
            return new List<Product>(productList);
        }

        public virtual bool AddProduct(Product p)
        {
            productList.Add(p);
            productList.Sort();
            return true;
        }

        public bool UpdateProductName(int id, string name)
        {
            // Returns true if product was updated.
            // First try to obtain the product by id.
            Product p = GetProduct(id);

            // If we obtained an actual product we can update its info.
            if (p != null)
            {
                p.Name = name;
                // Return true, indicating the change was successful.
                return true;
            }
            // Returns false if product was not updated (did not exist)
            return false;
        }
    }
}
