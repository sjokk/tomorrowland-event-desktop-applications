﻿using System;
using System.Collections.Generic;

namespace StandNItems
{
    public class Stand : EventStandManagement
    {
        // We actually did not design a specific stand object for the loanable products.
        // These would not need an ID nor a Name but would benefit from some of the methods.

        // Constructors
        public Stand() : base()
        { }

        // Methods
        // For the loanable product stand the ID will never conflict,
        // once the add button is pressed in the application, x number
        // of products will be added into the database against that
        // specified price, automatically incrementing the unique ID
        // in the database resulting in no conflict.

        // Same holds for the consumable products stand, the primary key
        // can not occur more than once in the database relation.
        
        public override bool AddProduct(Product p)
        {
            // Re-use existing method, duplicate will be either a product object or null.
            // Based on the existence of the id.
            Product duplicate = GetProduct(p.Id);
            
            // If there is no product with the id of product p, we add the product to the product list.
            if (duplicate == null)
            {
                base.AddProduct(p);
                return true;
            }
            // Otherwise a false value (failure) is returned.
            return false;
        }

        public bool UpdateProductPrice(int id, decimal price)
        {
            // Returns true if product was updated.
            // First try to obtain the product by id.
            Product p = GetProduct(id);

            // If we obtained an actual product we can update its info.
            if (p != null)
            {
                ((StandProduct)p).Price = price;
                // Return true, indicating the change was successful.
                return true;
            }
            // Returns false if product was not updated (did not exist)
            return false;
        }
    }
}
