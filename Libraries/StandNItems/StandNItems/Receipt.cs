﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StandNItems
{
    public class Receipt
    {
        // Fields
        private List<ReceiptLine> receiptLines = null;
        private int standId;

        // Constructor
        public Receipt(int standId)
        {
            receiptLines = new List<ReceiptLine>();
            this.standId = standId;
        }

        // Methods
        public void AddReceiptLine(ReceiptLine rl)
        {
            ReceiptLine rlDupe = GetReceiptLine(rl.ProductId);

            if (rlDupe != null)
            {
                int newSalesQuantity = rl.SalesQuantity + rlDupe.SalesQuantity;
                rlDupe.UpdateSalesQuantity(newSalesQuantity);
            }
            else
            {
                receiptLines.Add(rl);
            }
        }

        public void RemoveReceiptLine(ReceiptLine rl)
        {
            receiptLines.Remove(rl);
        }

        public ReceiptLine GetReceiptLine(int id)
        {
            foreach (ReceiptLine currentRl in GetReceiptLines())
            {
                if (id == currentRl.ProductId)
                {
                    return currentRl;
                }
            }
            return null;
        }

        public List<ReceiptLine> GetReceiptLines()
        {
            return new List<ReceiptLine>(receiptLines);
        }

        public decimal ReceiptTotal()
        {
            decimal receiptTotal = 0.0m;
            foreach (ReceiptLine rl in GetReceiptLines())
            {
                receiptTotal += rl.SubTotal;
            }
            return receiptTotal;
        }
        #region Database Access
        //public bool Save()
        //{
        //    // Insert-statement
        //    // Update-statement
        //}
        #endregion
    }
}
