﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StandNItems
{
    public class LoanableProduct : StandProduct
    {
        // Properties
        public int TicketId { get; private set; } // Depends on the logic that needs to be involved in assigning a ticketid to the product.

        // Constructors
        public LoanableProduct(int id, string name, decimal price) : base(id, name, price)
        {
            // Initialize with ticektId not being assigned.
        }

        // Methods
        public void ReturnProduct()
        {
            // Upon returning the product, we set the ticketid back to 0 so we can change the owner
            // by using the methods ChangeOwner.
            TicketId = 0;
        }

        public bool ChangeOwner(int ticketId)
        {
            if (TicketId == 0)
            {
                // In case there is no ticketId assigned to the product, we can change the ownership.
                TicketId = ticketId;
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            string lendInformation = "None";

            if (TicketId != 0)
            {
                lendInformation = TicketId.ToString();
            }
            // Return an appropriate information string that tells whether the loanable product
            // is lend out to a specific ticketid or not.
            return $"{base.ToString()}, {lendInformation}";
        }
    }
}
