﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StandNItems
{
    public class ReceiptLine
    {
        // Fields
        private decimal salesPrice;
        private int salesQuantity;

        // Properties
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal SalesPrice
        {
            get { return salesPrice; }
            private set
            {
                if (value >= 0.0m)
                {
                    salesPrice = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("SalesPrice", "SalesPrice can not be negative.");
                }
            }
        }
        public decimal SubTotal
        {
            get { return SalesPrice * SalesQuantity; }
        }

        public int SalesQuantity
        {
            get { return salesQuantity; }
            private set
            {
                if (value > 0)
                {
                    salesQuantity = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("SalesQuantity", "SalesQuantity has to be greater than 0.");
                }
            }
        }

        // Constructors
        public ReceiptLine (int productId, string productName, decimal salesPrice, int salesQuantity)
        {
            ProductId = productId;
            ProductName = productName;
            SalesPrice = salesPrice;
            SalesQuantity = salesQuantity;
        }

        // Methods
        public override string ToString()
        {
            return $"({ProductId}, {ProductName}, €{SalesPrice}, {SalesQuantity}, €{SubTotal})";
        }

        public void UpdateSalesQuantity(int newQuantity)
        {
            SalesQuantity = newQuantity;
        }
    }
}
