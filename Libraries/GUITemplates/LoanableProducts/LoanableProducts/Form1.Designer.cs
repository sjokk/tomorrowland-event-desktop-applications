﻿namespace LoanableProducts
{
    partial class productsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(productsForm));
            this.loanableProductGb = new System.Windows.Forms.GroupBox();
            this.priceLb = new System.Windows.Forms.Label();
            this.nameLb = new System.Windows.Forms.Label();
            this.idLb = new System.Windows.Forms.Label();
            this.addProductBtn = new System.Windows.Forms.Button();
            this.priceTb = new System.Windows.Forms.TextBox();
            this.nameTb = new System.Windows.Forms.TextBox();
            this.idTb = new System.Windows.Forms.TextBox();
            this.overviewGb = new System.Windows.Forms.GroupBox();
            this.productsOverviewLb = new System.Windows.Forms.ListBox();
            this.removeBtn = new System.Windows.Forms.Button();
            this.loanableProductGb.SuspendLayout();
            this.overviewGb.SuspendLayout();
            this.SuspendLayout();
            // 
            // loanableProductGb
            // 
            this.loanableProductGb.BackColor = System.Drawing.Color.Transparent;
            this.loanableProductGb.Controls.Add(this.priceLb);
            this.loanableProductGb.Controls.Add(this.nameLb);
            this.loanableProductGb.Controls.Add(this.idLb);
            this.loanableProductGb.Controls.Add(this.addProductBtn);
            this.loanableProductGb.Controls.Add(this.priceTb);
            this.loanableProductGb.Controls.Add(this.nameTb);
            this.loanableProductGb.Controls.Add(this.idTb);
            this.loanableProductGb.ForeColor = System.Drawing.Color.Black;
            this.loanableProductGb.Location = new System.Drawing.Point(6, 6);
            this.loanableProductGb.Margin = new System.Windows.Forms.Padding(2);
            this.loanableProductGb.Name = "loanableProductGb";
            this.loanableProductGb.Padding = new System.Windows.Forms.Padding(2);
            this.loanableProductGb.Size = new System.Drawing.Size(207, 150);
            this.loanableProductGb.TabIndex = 0;
            this.loanableProductGb.TabStop = false;
            this.loanableProductGb.Text = "Properties";
            // 
            // priceLb
            // 
            this.priceLb.AutoSize = true;
            this.priceLb.Location = new System.Drawing.Point(6, 84);
            this.priceLb.Name = "priceLb";
            this.priceLb.Size = new System.Drawing.Size(37, 13);
            this.priceLb.TabIndex = 9;
            this.priceLb.Text = "Price: ";
            // 
            // nameLb
            // 
            this.nameLb.AutoSize = true;
            this.nameLb.Location = new System.Drawing.Point(6, 60);
            this.nameLb.Name = "nameLb";
            this.nameLb.Size = new System.Drawing.Size(38, 13);
            this.nameLb.TabIndex = 8;
            this.nameLb.Text = "Name:";
            // 
            // idLb
            // 
            this.idLb.AutoSize = true;
            this.idLb.Location = new System.Drawing.Point(6, 36);
            this.idLb.Name = "idLb";
            this.idLb.Size = new System.Drawing.Size(24, 13);
            this.idLb.TabIndex = 7;
            this.idLb.Text = "ID: ";
            // 
            // addProductBtn
            // 
            this.addProductBtn.ForeColor = System.Drawing.Color.Black;
            this.addProductBtn.Location = new System.Drawing.Point(9, 113);
            this.addProductBtn.Margin = new System.Windows.Forms.Padding(2);
            this.addProductBtn.Name = "addProductBtn";
            this.addProductBtn.Size = new System.Drawing.Size(187, 22);
            this.addProductBtn.TabIndex = 6;
            this.addProductBtn.Text = "Add Product";
            this.addProductBtn.UseVisualStyleBackColor = true;
            // 
            // priceTb
            // 
            this.priceTb.Location = new System.Drawing.Point(48, 77);
            this.priceTb.Margin = new System.Windows.Forms.Padding(2);
            this.priceTb.Name = "priceTb";
            this.priceTb.ReadOnly = true;
            this.priceTb.Size = new System.Drawing.Size(148, 20);
            this.priceTb.TabIndex = 2;
            this.priceTb.Text = "Price";
            // 
            // nameTb
            // 
            this.nameTb.Location = new System.Drawing.Point(49, 53);
            this.nameTb.Margin = new System.Windows.Forms.Padding(2);
            this.nameTb.Name = "nameTb";
            this.nameTb.ReadOnly = true;
            this.nameTb.Size = new System.Drawing.Size(147, 20);
            this.nameTb.TabIndex = 1;
            this.nameTb.Text = "Name";
            // 
            // idTb
            // 
            this.idTb.Location = new System.Drawing.Point(49, 29);
            this.idTb.Margin = new System.Windows.Forms.Padding(2);
            this.idTb.Name = "idTb";
            this.idTb.Size = new System.Drawing.Size(147, 20);
            this.idTb.TabIndex = 0;
            this.idTb.Text = "id";
            // 
            // overviewGb
            // 
            this.overviewGb.BackColor = System.Drawing.Color.Transparent;
            this.overviewGb.Controls.Add(this.productsOverviewLb);
            this.overviewGb.ForeColor = System.Drawing.Color.Black;
            this.overviewGb.Location = new System.Drawing.Point(217, 6);
            this.overviewGb.Margin = new System.Windows.Forms.Padding(2);
            this.overviewGb.Name = "overviewGb";
            this.overviewGb.Padding = new System.Windows.Forms.Padding(2);
            this.overviewGb.Size = new System.Drawing.Size(320, 394);
            this.overviewGb.TabIndex = 4;
            this.overviewGb.TabStop = false;
            this.overviewGb.Text = "Overview";
            // 
            // productsOverviewLb
            // 
            this.productsOverviewLb.FormattingEnabled = true;
            this.productsOverviewLb.Location = new System.Drawing.Point(8, 88);
            this.productsOverviewLb.Margin = new System.Windows.Forms.Padding(2);
            this.productsOverviewLb.Name = "productsOverviewLb";
            this.productsOverviewLb.Size = new System.Drawing.Size(307, 251);
            this.productsOverviewLb.TabIndex = 1;
            // 
            // removeBtn
            // 
            this.removeBtn.ForeColor = System.Drawing.Color.Black;
            this.removeBtn.Location = new System.Drawing.Point(15, 209);
            this.removeBtn.Margin = new System.Windows.Forms.Padding(2);
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(187, 22);
            this.removeBtn.TabIndex = 7;
            this.removeBtn.Text = "Remove Product";
            this.removeBtn.UseVisualStyleBackColor = true;
            // 
            // productsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(544, 411);
            this.Controls.Add(this.removeBtn);
            this.Controls.Add(this.overviewGb);
            this.Controls.Add(this.loanableProductGb);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(517, 450);
            this.Name = "productsForm";
            this.Text = "Product Application";
            this.loanableProductGb.ResumeLayout(false);
            this.loanableProductGb.PerformLayout();
            this.overviewGb.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        protected System.Windows.Forms.GroupBox loanableProductGb;
        protected System.Windows.Forms.GroupBox overviewGb;
        protected System.Windows.Forms.ListBox productsOverviewLb;
        protected System.Windows.Forms.TextBox priceTb;
        protected System.Windows.Forms.TextBox nameTb;
        protected System.Windows.Forms.TextBox idTb;
        protected System.Windows.Forms.Button addProductBtn;
        protected System.Windows.Forms.Button removeBtn;
        protected System.Windows.Forms.Label priceLb;
        protected System.Windows.Forms.Label nameLb;
        protected System.Windows.Forms.Label idLb;
    }
}

