﻿namespace standManagement
{
    partial class standManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(standManagementForm));
            this.productInsertionGbx = new System.Windows.Forms.GroupBox();
            this.stockLb = new System.Windows.Forms.Label();
            this.priceLb = new System.Windows.Forms.Label();
            this.nameLb = new System.Windows.Forms.Label();
            this.idLb = new System.Windows.Forms.Label();
            this.stockTb = new System.Windows.Forms.TextBox();
            this.priceTb = new System.Windows.Forms.TextBox();
            this.nameTb = new System.Windows.Forms.TextBox();
            this.idTb = new System.Windows.Forms.TextBox();
            this.productOverview = new System.Windows.Forms.ListBox();
            this.addBtn = new System.Windows.Forms.Button();
            this.updateBtn = new System.Windows.Forms.Button();
            this.removeBtn = new System.Windows.Forms.Button();
            this.applicationPanel = new System.Windows.Forms.Panel();
            this.productInsertionGbx.SuspendLayout();
            this.applicationPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // productInsertionGbx
            // 
            this.productInsertionGbx.BackColor = System.Drawing.Color.Transparent;
            this.productInsertionGbx.Controls.Add(this.stockLb);
            this.productInsertionGbx.Controls.Add(this.priceLb);
            this.productInsertionGbx.Controls.Add(this.nameLb);
            this.productInsertionGbx.Controls.Add(this.idLb);
            this.productInsertionGbx.Controls.Add(this.stockTb);
            this.productInsertionGbx.Controls.Add(this.priceTb);
            this.productInsertionGbx.Controls.Add(this.nameTb);
            this.productInsertionGbx.Controls.Add(this.idTb);
            this.productInsertionGbx.Location = new System.Drawing.Point(8, 6);
            this.productInsertionGbx.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.productInsertionGbx.Name = "productInsertionGbx";
            this.productInsertionGbx.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.productInsertionGbx.Size = new System.Drawing.Size(254, 145);
            this.productInsertionGbx.TabIndex = 0;
            this.productInsertionGbx.TabStop = false;
            this.productInsertionGbx.Text = "Product Management";
            // 
            // stockLb
            // 
            this.stockLb.AutoSize = true;
            this.stockLb.Location = new System.Drawing.Point(5, 123);
            this.stockLb.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.stockLb.Name = "stockLb";
            this.stockLb.Size = new System.Drawing.Size(91, 13);
            this.stockLb.TabIndex = 6;
            this.stockLb.Text = "Quantity in Stock:";
            // 
            // priceLb
            // 
            this.priceLb.AutoSize = true;
            this.priceLb.Location = new System.Drawing.Point(5, 93);
            this.priceLb.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.priceLb.Name = "priceLb";
            this.priceLb.Size = new System.Drawing.Size(34, 13);
            this.priceLb.TabIndex = 5;
            this.priceLb.Text = "Price:";
            // 
            // nameLb
            // 
            this.nameLb.AutoSize = true;
            this.nameLb.Location = new System.Drawing.Point(4, 62);
            this.nameLb.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nameLb.Name = "nameLb";
            this.nameLb.Size = new System.Drawing.Size(38, 13);
            this.nameLb.TabIndex = 4;
            this.nameLb.Text = "Name:";
            // 
            // idLb
            // 
            this.idLb.AutoSize = true;
            this.idLb.Location = new System.Drawing.Point(5, 34);
            this.idLb.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.idLb.Name = "idLb";
            this.idLb.Size = new System.Drawing.Size(21, 13);
            this.idLb.TabIndex = 1;
            this.idLb.Text = "ID:";
            // 
            // stockTb
            // 
            this.stockTb.Location = new System.Drawing.Point(100, 116);
            this.stockTb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.stockTb.Name = "stockTb";
            this.stockTb.Size = new System.Drawing.Size(145, 20);
            this.stockTb.TabIndex = 3;
            // 
            // priceTb
            // 
            this.priceTb.Location = new System.Drawing.Point(100, 86);
            this.priceTb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.priceTb.Name = "priceTb";
            this.priceTb.Size = new System.Drawing.Size(145, 20);
            this.priceTb.TabIndex = 2;
            // 
            // nameTb
            // 
            this.nameTb.Location = new System.Drawing.Point(100, 55);
            this.nameTb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.nameTb.Name = "nameTb";
            this.nameTb.Size = new System.Drawing.Size(145, 20);
            this.nameTb.TabIndex = 1;
            // 
            // idTb
            // 
            this.idTb.Location = new System.Drawing.Point(100, 27);
            this.idTb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.idTb.Name = "idTb";
            this.idTb.ReadOnly = true;
            this.idTb.Size = new System.Drawing.Size(145, 20);
            this.idTb.TabIndex = 0;
            // 
            // productOverview
            // 
            this.productOverview.FormattingEnabled = true;
            this.productOverview.Location = new System.Drawing.Point(8, 158);
            this.productOverview.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.productOverview.Name = "productOverview";
            this.productOverview.Size = new System.Drawing.Size(420, 251);
            this.productOverview.TabIndex = 1;
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(266, 37);
            this.addBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(162, 27);
            this.addBtn.TabIndex = 3;
            this.addBtn.Text = "Add";
            this.addBtn.UseVisualStyleBackColor = true;
            // 
            // updateBtn
            // 
            this.updateBtn.Location = new System.Drawing.Point(266, 68);
            this.updateBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(162, 26);
            this.updateBtn.TabIndex = 4;
            this.updateBtn.Text = "Update";
            this.updateBtn.UseVisualStyleBackColor = true;
            // 
            // removeBtn
            // 
            this.removeBtn.Location = new System.Drawing.Point(266, 98);
            this.removeBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(162, 27);
            this.removeBtn.TabIndex = 5;
            this.removeBtn.Text = "Remove";
            this.removeBtn.UseVisualStyleBackColor = true;
            // 
            // applicationPanel
            // 
            this.applicationPanel.Controls.Add(this.productInsertionGbx);
            this.applicationPanel.Controls.Add(this.removeBtn);
            this.applicationPanel.Controls.Add(this.addBtn);
            this.applicationPanel.Controls.Add(this.updateBtn);
            this.applicationPanel.Controls.Add(this.productOverview);
            this.applicationPanel.Location = new System.Drawing.Point(0, 3);
            this.applicationPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.applicationPanel.Name = "applicationPanel";
            this.applicationPanel.Size = new System.Drawing.Size(436, 420);
            this.applicationPanel.TabIndex = 6;
            // 
            // standManagementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(439, 426);
            this.Controls.Add(this.applicationPanel);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "standManagementForm";
            this.Text = "Stand Management Application";
            this.productInsertionGbx.ResumeLayout(false);
            this.productInsertionGbx.PerformLayout();
            this.applicationPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        protected System.Windows.Forms.GroupBox productInsertionGbx;
        protected System.Windows.Forms.ListBox productOverview;
        protected System.Windows.Forms.Button addBtn;
        protected System.Windows.Forms.Button updateBtn;
        protected System.Windows.Forms.Button removeBtn;
        protected System.Windows.Forms.TextBox stockTb;
        protected System.Windows.Forms.TextBox priceTb;
        protected System.Windows.Forms.TextBox nameTb;
        protected System.Windows.Forms.TextBox idTb;
        protected System.Windows.Forms.Label stockLb;
        protected System.Windows.Forms.Label priceLb;
        protected System.Windows.Forms.Label nameLb;
        protected System.Windows.Forms.Label idLb;
        protected System.Windows.Forms.Panel applicationPanel;
    }
}

