﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using DataBaseMethods;

namespace Event_Statistics
{
    class CustomerManagement : DataBase
    {
        public List<Customer> customers { get; set; }

        public CustomerManagement()
        {
            customers = new List<Customer>();
        }
        
        public Customer getCustomer(int cusid, int ticketid, int eventid)
        {
            foreach (Customer c in customers)
            {
                if (c.CustomerId == cusid && c.TicketId == ticketid && c.EventId == eventid)
                {
                    return c;
                }
            }
            return null;
        }

        //retrieve customer data from database and show them in the list
        public void RetrieveAllDataFromDB()
        {
            string sql = "select * from ticket where IsValid = 1";

            MySqlDataReader mdr = ExecuteReader(sql);

            while (mdr.Read())
            {
                int cusId = Convert.ToInt32(mdr[2]);
                int ticketId = Convert.ToInt32(mdr[0]);
                double balance = Convert.ToDouble(mdr[1]);
                int eventId = Convert.ToInt32(mdr[3]);
                /*Since it gave an error while trying to retrieve NULL value from database,
                 * use if...else statement: If the value is not null, show this value, otherwise
                 * an 0 is given.
                 * Try...catch is also a solution but it's not preferrable.
                */
                int cpId;
                if (!mdr.IsDBNull(4))
                {
                    cpId = Convert.ToInt32(mdr[4]);
                }
                else
                {
                    cpId = 0;
                }
                //try
                //{
                //    cpId = Convert.ToInt32(mdr[4]);
                //}
                //catch
                //{
                //    cpId = 0;
                //}
                int ispresent = Convert.ToInt32(mdr[5]);

                Customer c = new Customer(cusId, ticketId, balance, eventId, cpId, ispresent);
                customers.Add(c);
            }
            CloseConnection();
        }

        public List<Customer> GetAllCustomers()
        {
            return customers;
        }
    }
}
