﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using DataBaseMethods;

namespace Event_Statistics
{
    class ShopManagement : DataBase
    {
        public List<Shop> shops { get; set; }

        public ShopManagement()
        {
            shops = new List<Shop>();
        }

        public Shop getShop(int standid, int productid)
        {
            foreach(Shop s in GetAllShops())
            {
                if (s.StandId == standid && s.ProductId == productid)
                {
                    return s;
                }
            }
            return null;
        }

        //retrieve data of products in shops from database and show them in the list
        public void RetrieveAllDataFromDB()
        {
            string sql = "select * from stand_has_consumable_product where IsRemoved = 0";

            MySqlDataReader mdr = ExecuteReader(sql);

            while (mdr.Read())
            {
                int standId = Convert.ToInt32(mdr[0]);
                int productId = Convert.ToInt32(mdr[1]);
                double price = Convert.ToDouble(mdr[2]);
                int numberInstock = Convert.ToInt32(mdr[3]);

                Shop s = new Shop(standId, productId, price, numberInstock);
                shops.Add(s);
            }
            CloseConnection();
        }

        public List<Shop> GetAllShops()
        {
            return shops;
        }
    }
}
