﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Event_Statistics
{
    public partial class Form1 : Form
    {
        private CustomerManagement cm;
        private ShopManagement sm;
        private EventManagement em;
        public Form1()
        {
            InitializeComponent();
            cm = new CustomerManagement();
            sm = new ShopManagement();
            em = new EventManagement();
        }

        private void ShowVistorInfobtn_Click(object sender, EventArgs e)
        {
            lbxVistorInfo.Items.Clear();
            try
            {
                int inputCustomerid = Convert.ToInt32(customerIdtbx.Text);
                int inputTicketid = Convert.ToInt32(ticketIdtbx.Text);
                int inputEventid = Convert.ToInt32(eventIdtbx.Text);
                Customer c = cm.getCustomer(inputCustomerid, inputTicketid, inputEventid);

                if (c != null)
                {
                    lbxVistorInfo.Items.Add(c);
                }
                else
                {
                    MessageBox.Show("No matching visitor!");
                }
            }
            catch(FormatException)
            {
                MessageBox.Show("Please enter valid ids or input correct company name and password first!");
            }
            catch(EventException ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void btnShowProductInfo_Click(object sender, EventArgs e)
        {
            lbxProductInfo.Items.Clear();
            try
            {
                int inputstandId = Convert.ToInt32(StandIdtbx.Text);
                int inputproductId = Convert.ToInt32(ProductIdtbx.Text);
                Shop s = sm.getShop(inputstandId, inputproductId);

                if (s != null)
                {
                    lbxProductInfo.Items.Add(s);
                }
                else
                {
                    MessageBox.Show("No matching product!");
                }
            }
            catch(FormatException)
            {
                MessageBox.Show("Please enter valid ids or enter correct company name and password first!");
            }
            catch (EventException ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void btnComInspect_Click(object sender, EventArgs e)
        {
            try
            {
                em.RetrieveDataOfCompanyFromDB();
                String companyName = CompanyNametbx.Text;
                int password = Convert.ToInt32(Passwordtbx.Text);
                Event E = em.getCompany(companyName, password);
                /*check if the company name and password are valid and
                 * are corresponding to each other. if so, two other parts 
                 * will be visible and related info will be shown in the lbx
                */
                if (E != null)
                {
                    EnableAllgbxes();

                    em.RetrieveDataOfEventFromDB();
                    foreach (Event ev in em.getAllEvents())
                    {
                        lbxEventStatistics.Items.Add(ev);
                    }

                    cm.RetrieveAllDataFromDB();
                    foreach (Customer c in cm.GetAllCustomers())
                    {
                        lbxVistorInfo.Items.Add(c);
                    }

                    sm.RetrieveAllDataFromDB();
                    foreach (Shop s in sm.GetAllShops())
                    {
                        lbxProductInfo.Items.Add(s);
                    }
                }
                else
                {
                    MessageBox.Show("no matching company name and password!");
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Please enter valid company name and password!");
            }
            catch (EventException ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        //enable visitor and shop groupboxes only if the employee enters correct company name and password
        private void EnableAllgbxes()
        {
            groupBox1.Enabled = true;
            groupBox3.Enabled = true;
        }
        //load all the info again after searching for a single row of data
        private void LoadAllVisitorsbtn_Click(object sender, EventArgs e)
        {
            foreach (Customer c in cm.GetAllCustomers())
            {
                lbxVistorInfo.Items.Add(c);
            }
        }
        private void LoadAllProductsbtn_Click(object sender, EventArgs e)
        {
            foreach (Shop s in sm.GetAllShops())
            {
                lbxProductInfo.Items.Add(s);
            }
        }
    }
}
