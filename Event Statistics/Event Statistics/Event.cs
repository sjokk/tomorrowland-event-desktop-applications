﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event_Statistics
{
    class Event
    {
        public String CompanyName { get; set; }
        public int Password { get; set; }
        public int EventId { get; private set; }
        public String EventName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Price { get; set; }
        //public int IsActivated { get; set; }

        public Event(String companyName, int password)
        {
            CompanyName = companyName;
            Password = password;
        }

        public Event(int eventId, String eventName, DateTime startDate, DateTime endDate, int price)
        {
            EventId = eventId;
            EventName = eventName;
            StartDate = startDate;
            EndDate = endDate;
            Price = price;
        }

        public override string ToString()
        {
            return "Event id: " + EventId + " Event name: " + EventName +
                " Start date: " + StartDate + " End date: " + EndDate +
                " Price: " + Price;
        }
    }
}
