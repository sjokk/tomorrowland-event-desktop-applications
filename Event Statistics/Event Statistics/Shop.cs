﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event_Statistics
{
    class Shop
    {
        //property
        public int StandId { get; private set; }
        public int ProductId { get; set; }
        public double Price { get; set; }
        public int NumberInStock { get; set; }
        
        //constructor
        public Shop(int standId, int productId, double price, int numberInStock)
        {
            StandId = standId;
            ProductId = productId;
            Price = price;
            NumberInStock = numberInStock;
        }
        //method
        public override string ToString()
        {
            return "Price: $ " + Price + ", number in stock: " + NumberInStock;
        }
    }
}
