﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using DataBaseMethods;

namespace Event_Statistics
{
    class EventManagement : DataBase
    {
        public List<Event> events { get; set; }
        public List<Event> companies { get; set; }
        public EventManagement()
        {
            events = new List<Event>();
            companies = new List<Event>();
        }

        public Event getCompany(String compName, int pW)
        {
            foreach (Event e in companies)
            {
                if (e.CompanyName == compName && e.Password == pW)
                {
                    return e;
                }
            }
            return null;
        }

        public void RetrieveDataOfEventFromDB()
        {
            string sql = "select * from event";

            MySqlDataReader mdr = ExecuteReader(sql);

            while (mdr.Read())
            {
                int eventId = Convert.ToInt32(mdr[0]);
                String eventName = Convert.ToString(mdr[1]);
                DateTime startDate = Convert.ToDateTime(mdr[2]);
                DateTime endDate = Convert.ToDateTime(mdr[3]);
                int price = Convert.ToInt32(mdr[4]);

                Event e = new Event(eventId, eventName, startDate, endDate, price);
                events.Add(e);
            }
            CloseConnection();
        }

        public void RetrieveDataOfCompanyFromDB()
        {
            string sql = "select * from company";

            MySqlDataReader mdr = ExecuteReader(sql);

            while (mdr.Read())
            {
                String companyName = Convert.ToString(mdr[1]);
                int password = Convert.ToInt32(mdr[2]);

                Event e = new Event(companyName, password);
                companies.Add(e);
            }
            CloseConnection();
        }

        public List<Event> getAllEvents()
        {
            return events;
        }

        public List<Event> getAllCompanies()
        {
            return companies;
        }
    }
}
