﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event_Statistics
{
    class Customer
    {
        //property
        public int CustomerId { get; private set; }
        public int TicketId { get; set; } //store the id whose ticket is held by a specific customer
        public double Balance { get; set; }
        public int EventId { get; set; } //store the event id which the customer is currently in
        public int CampingspotId { get; set; }
        public int IsPresent { get; set; }
        //public int IsValid { get; set; } //check if the ticket which is currently held by the customer is valid
        //constructor
        public Customer(int customerId, int ticketId, double balance, int eventId, int campingspotId, int isPresent)
        {
            CustomerId = customerId;
            TicketId = ticketId;
            Balance = balance;
            EventId = eventId;
            CampingspotId = campingspotId;
            IsPresent = isPresent;
            //IsValid = isValid;
        }
        //method
        public override string ToString()
        {
            if (IsPresent == 1)
            {
                return "He or she is on campingspot "
                + CampingspotId + ", with balance $" + Balance;
            }
            return "He or she has already left the event";
        }
    }
}
