﻿namespace Event_Statistics
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ShowVistorInfobtn = new System.Windows.Forms.Button();
            this.lbxVistorInfo = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.customerIdtbx = new System.Windows.Forms.TextBox();
            this.ticketIdtbx = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.eventIdtbx = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbxEventStatistics = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnComInspect = new System.Windows.Forms.Button();
            this.Passwordtbx = new System.Windows.Forms.TextBox();
            this.CompanyNametbx = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbxProductInfo = new System.Windows.Forms.ListBox();
            this.StandIdtbx = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnShowProductInfo = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.ProductIdtbx = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.LoadAllVisitorsbtn = new System.Windows.Forms.Button();
            this.LoadAllProductsbtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // ShowVistorInfobtn
            // 
            this.ShowVistorInfobtn.Location = new System.Drawing.Point(10, 170);
            this.ShowVistorInfobtn.Name = "ShowVistorInfobtn";
            this.ShowVistorInfobtn.Size = new System.Drawing.Size(143, 51);
            this.ShowVistorInfobtn.TabIndex = 0;
            this.ShowVistorInfobtn.Text = "Show info";
            this.ShowVistorInfobtn.UseVisualStyleBackColor = true;
            this.ShowVistorInfobtn.Click += new System.EventHandler(this.ShowVistorInfobtn_Click);
            // 
            // lbxVistorInfo
            // 
            this.lbxVistorInfo.FormattingEnabled = true;
            this.lbxVistorInfo.ItemHeight = 20;
            this.lbxVistorInfo.Location = new System.Drawing.Point(10, 230);
            this.lbxVistorInfo.Name = "lbxVistorInfo";
            this.lbxVistorInfo.Size = new System.Drawing.Size(294, 424);
            this.lbxVistorInfo.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "ID: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ticket ID:";
            // 
            // customerIdtbx
            // 
            this.customerIdtbx.Location = new System.Drawing.Point(93, 39);
            this.customerIdtbx.Name = "customerIdtbx";
            this.customerIdtbx.Size = new System.Drawing.Size(136, 26);
            this.customerIdtbx.TabIndex = 5;
            // 
            // ticketIdtbx
            // 
            this.ticketIdtbx.Location = new System.Drawing.Point(93, 79);
            this.ticketIdtbx.Name = "ticketIdtbx";
            this.ticketIdtbx.Size = new System.Drawing.Size(136, 26);
            this.ticketIdtbx.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.LoadAllVisitorsbtn);
            this.groupBox1.Controls.Add(this.eventIdtbx);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbxVistorInfo);
            this.groupBox1.Controls.Add(this.customerIdtbx);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.ticketIdtbx);
            this.groupBox1.Controls.Add(this.ShowVistorInfobtn);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(12, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(310, 680);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Visitor Information";
            // 
            // eventIdtbx
            // 
            this.eventIdtbx.Location = new System.Drawing.Point(93, 118);
            this.eventIdtbx.Name = "eventIdtbx";
            this.eventIdtbx.Size = new System.Drawing.Size(136, 26);
            this.eventIdtbx.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Event ID:";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.lbxEventStatistics);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.btnComInspect);
            this.groupBox2.Controls.Add(this.Passwordtbx);
            this.groupBox2.Controls.Add(this.CompanyNametbx);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(328, 14);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(323, 680);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Event Inspect";
            // 
            // lbxEventStatistics
            // 
            this.lbxEventStatistics.FormattingEnabled = true;
            this.lbxEventStatistics.ItemHeight = 20;
            this.lbxEventStatistics.Location = new System.Drawing.Point(10, 270);
            this.lbxEventStatistics.Name = "lbxEventStatistics";
            this.lbxEventStatistics.Size = new System.Drawing.Size(307, 384);
            this.lbxEventStatistics.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 242);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(185, 20);
            this.label8.TabIndex = 9;
            this.label8.Text = "Overall event statistics:";
            // 
            // btnComInspect
            // 
            this.btnComInspect.Location = new System.Drawing.Point(10, 161);
            this.btnComInspect.Name = "btnComInspect";
            this.btnComInspect.Size = new System.Drawing.Size(210, 51);
            this.btnComInspect.TabIndex = 8;
            this.btnComInspect.Text = "Inspect";
            this.btnComInspect.UseVisualStyleBackColor = true;
            this.btnComInspect.Click += new System.EventHandler(this.btnComInspect_Click);
            // 
            // Passwordtbx
            // 
            this.Passwordtbx.Location = new System.Drawing.Point(165, 95);
            this.Passwordtbx.Name = "Passwordtbx";
            this.Passwordtbx.Size = new System.Drawing.Size(143, 26);
            this.Passwordtbx.TabIndex = 7;
            // 
            // CompanyNametbx
            // 
            this.CompanyNametbx.Location = new System.Drawing.Point(165, 44);
            this.CompanyNametbx.Name = "CompanyNametbx";
            this.CompanyNametbx.Size = new System.Drawing.Size(143, 26);
            this.CompanyNametbx.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 20);
            this.label7.TabIndex = 5;
            this.label7.Text = "Password:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 20);
            this.label6.TabIndex = 4;
            this.label6.Text = "Company name:";
            // 
            // lbxProductInfo
            // 
            this.lbxProductInfo.FormattingEnabled = true;
            this.lbxProductInfo.ItemHeight = 20;
            this.lbxProductInfo.Location = new System.Drawing.Point(10, 241);
            this.lbxProductInfo.Name = "lbxProductInfo";
            this.lbxProductInfo.Size = new System.Drawing.Size(301, 424);
            this.lbxProductInfo.TabIndex = 23;
            // 
            // StandIdtbx
            // 
            this.StandIdtbx.Location = new System.Drawing.Point(101, 44);
            this.StandIdtbx.Name = "StandIdtbx";
            this.StandIdtbx.Size = new System.Drawing.Size(158, 26);
            this.StandIdtbx.TabIndex = 22;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 50);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 20);
            this.label13.TabIndex = 21;
            this.label13.Text = "Stand ID:";
            // 
            // btnShowProductInfo
            // 
            this.btnShowProductInfo.Location = new System.Drawing.Point(10, 161);
            this.btnShowProductInfo.Name = "btnShowProductInfo";
            this.btnShowProductInfo.Size = new System.Drawing.Size(144, 51);
            this.btnShowProductInfo.TabIndex = 19;
            this.btnShowProductInfo.Text = "Show info";
            this.btnShowProductInfo.UseVisualStyleBackColor = true;
            this.btnShowProductInfo.Click += new System.EventHandler(this.btnShowProductInfo_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 101);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 20);
            this.label11.TabIndex = 18;
            this.label11.Text = "Product ID:";
            // 
            // ProductIdtbx
            // 
            this.ProductIdtbx.Location = new System.Drawing.Point(101, 98);
            this.ProductIdtbx.Name = "ProductIdtbx";
            this.ProductIdtbx.Size = new System.Drawing.Size(158, 26);
            this.ProductIdtbx.TabIndex = 16;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.LoadAllProductsbtn);
            this.groupBox3.Controls.Add(this.lbxProductInfo);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.btnShowProductInfo);
            this.groupBox3.Controls.Add(this.StandIdtbx);
            this.groupBox3.Controls.Add(this.ProductIdtbx);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Enabled = false;
            this.groupBox3.Location = new System.Drawing.Point(657, 14);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(317, 680);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Product statistics";
            // 
            // LoadAllVisitorsbtn
            // 
            this.LoadAllVisitorsbtn.Location = new System.Drawing.Point(159, 170);
            this.LoadAllVisitorsbtn.Name = "LoadAllVisitorsbtn";
            this.LoadAllVisitorsbtn.Size = new System.Drawing.Size(143, 51);
            this.LoadAllVisitorsbtn.TabIndex = 9;
            this.LoadAllVisitorsbtn.Text = "Load all";
            this.LoadAllVisitorsbtn.UseVisualStyleBackColor = true;
            this.LoadAllVisitorsbtn.Click += new System.EventHandler(this.LoadAllVisitorsbtn_Click);
            // 
            // LoadAllProductsbtn
            // 
            this.LoadAllProductsbtn.Location = new System.Drawing.Point(160, 161);
            this.LoadAllProductsbtn.Name = "LoadAllProductsbtn";
            this.LoadAllProductsbtn.Size = new System.Drawing.Size(151, 51);
            this.LoadAllProductsbtn.TabIndex = 24;
            this.LoadAllProductsbtn.Text = "Load all";
            this.LoadAllProductsbtn.UseVisualStyleBackColor = true;
            this.LoadAllProductsbtn.Click += new System.EventHandler(this.LoadAllProductsbtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(986, 706);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.142858F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "x";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ShowVistorInfobtn;
        private System.Windows.Forms.ListBox lbxVistorInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox customerIdtbx;
        private System.Windows.Forms.TextBox ticketIdtbx;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox lbxEventStatistics;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnComInspect;
        private System.Windows.Forms.TextBox Passwordtbx;
        private System.Windows.Forms.TextBox CompanyNametbx;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnShowProductInfo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox ProductIdtbx;
        private System.Windows.Forms.ListBox lbxProductInfo;
        private System.Windows.Forms.TextBox StandIdtbx;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox eventIdtbx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button LoadAllVisitorsbtn;
        private System.Windows.Forms.Button LoadAllProductsbtn;
    }
}

