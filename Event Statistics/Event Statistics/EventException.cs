﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event_Statistics
{
    class EventException : Exception
    {
        public EventException(String m) : base(m) { }
    }
}
