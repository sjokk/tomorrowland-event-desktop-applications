﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StandNItems;

namespace LoanableProductsManagement
{
    public partial class loanableManagementForm : Form
    {
        private EventStandManagement myEventStandManagement;
        public loanableManagementForm()
        {
            InitializeComponent();
            InitializeLoanableProducts();
        }

        private void InitializeLoanableProducts()
        {
            List<Product> myLoanableProducts = ProductManagmentDBMethods.GetLoanableProducts();

            foreach (Product p in myLoanableProducts)
            {
                myEventStandManagement.AddProduct(p);
            }

            foreach (Product p in myEventStandManagement.GetProducts())
            {
                loanableProductsLbx.Items.Add(p);
            }
        }
    }
}
