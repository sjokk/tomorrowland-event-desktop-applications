﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBaseMethods;
using MySql.Data.MySqlClient;
using StandNItems;

namespace LoanableProductsManagement
{
    class ProductManagmentDBMethods : DataBase
    {
        public static List<Product> GetLoanableProducts()
        {
            string sql = "SELECT * FROM loanable_product_names WHERE IsRemoved = 0";
            MySqlDataReader rdr = ExecuteReader(sql);

            int id;
            string name;

            List<Product> resultList = new List<Product>();

            while (rdr.Read())
            {
                id = Convert.ToInt32(rdr[0]);
                name = rdr[1].ToString();

                resultList.Add(new Product(id, name));
            }

            return resultList;
        }
    }
}
