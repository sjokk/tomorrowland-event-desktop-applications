﻿namespace LoanableProductsManagement
{
    partial class loanableManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(loanableManagementForm));
            this.idLb = new System.Windows.Forms.Label();
            this.addBtn = new System.Windows.Forms.Button();
            this.idTb = new System.Windows.Forms.TextBox();
            this.loanableProductsLbx = new System.Windows.Forms.ListBox();
            this.nameLb = new System.Windows.Forms.Label();
            this.nameTb = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.removeBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // idLb
            // 
            this.idLb.AutoSize = true;
            this.idLb.Location = new System.Drawing.Point(25, 27);
            this.idLb.Name = "idLb";
            this.idLb.Size = new System.Drawing.Size(38, 25);
            this.idLb.TabIndex = 0;
            this.idLb.Text = "ID:";
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(12, 130);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(511, 51);
            this.addBtn.TabIndex = 1;
            this.addBtn.Text = "Add Product";
            this.addBtn.UseVisualStyleBackColor = true;
            // 
            // idTb
            // 
            this.idTb.Location = new System.Drawing.Point(149, 27);
            this.idTb.Name = "idTb";
            this.idTb.Size = new System.Drawing.Size(374, 31);
            this.idTb.TabIndex = 2;
            // 
            // loanableProductsLbx
            // 
            this.loanableProductsLbx.FormattingEnabled = true;
            this.loanableProductsLbx.ItemHeight = 25;
            this.loanableProductsLbx.Location = new System.Drawing.Point(12, 204);
            this.loanableProductsLbx.Name = "loanableProductsLbx";
            this.loanableProductsLbx.Size = new System.Drawing.Size(511, 529);
            this.loanableProductsLbx.TabIndex = 3;
            // 
            // nameLb
            // 
            this.nameLb.AutoSize = true;
            this.nameLb.Location = new System.Drawing.Point(25, 70);
            this.nameLb.Name = "nameLb";
            this.nameLb.Size = new System.Drawing.Size(74, 25);
            this.nameLb.TabIndex = 4;
            this.nameLb.Text = "Name:";
            // 
            // nameTb
            // 
            this.nameTb.Location = new System.Drawing.Point(149, 70);
            this.nameTb.Name = "nameTb";
            this.nameTb.Size = new System.Drawing.Size(374, 31);
            this.nameTb.TabIndex = 5;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(538, 204);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(313, 51);
            this.button2.TabIndex = 6;
            this.button2.Text = "Update Product";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // removeBtn
            // 
            this.removeBtn.Location = new System.Drawing.Point(538, 277);
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(313, 51);
            this.removeBtn.TabIndex = 7;
            this.removeBtn.Text = "Remove Product";
            this.removeBtn.UseVisualStyleBackColor = true;
            // 
            // loanableManagementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(871, 752);
            this.Controls.Add(this.removeBtn);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.nameTb);
            this.Controls.Add(this.nameLb);
            this.Controls.Add(this.loanableProductsLbx);
            this.Controls.Add(this.idTb);
            this.Controls.Add(this.addBtn);
            this.Controls.Add(this.idLb);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "loanableManagementForm";
            this.Text = "Loanable Products Management Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label idLb;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.TextBox idTb;
        private System.Windows.Forms.ListBox loanableProductsLbx;
        private System.Windows.Forms.Label nameLb;
        private System.Windows.Forms.TextBox nameTb;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button removeBtn;
    }
}

