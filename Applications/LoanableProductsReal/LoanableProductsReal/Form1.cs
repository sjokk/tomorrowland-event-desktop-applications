﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LoanableProducts;

namespace LoanableProductsReal
{
    public partial class loanableProductForm : productsForm
    {
        LoanableProductsStandManagement lpsmForm;
        //LoanableProductManagement p;
        public loanableProductForm()
        {
            InitializeComponent();
            //p = new LoanableProductManagement();
            
        }

        private void returnRbtn_CheckedChanged(object sender, EventArgs e)
        {
            confirmBtn.Text = "Return Products";
        }

        private void lendOutRbtn_CheckedChanged(object sender, EventArgs e)
        {
            confirmBtn.Text = "Lend Out Products";
        }

        public void standManagementBtn_Click(object sender, EventArgs e)
        {
            if (lpsmForm != null)
            {
                lpsmForm.Close();
            }
            lpsmForm = new LoanableProductsStandManagement(this);
            lpsmForm.Show();
            
        }

        private void addProductBtn_Click(object sender, EventArgs e)
        {

        }
    }
}
