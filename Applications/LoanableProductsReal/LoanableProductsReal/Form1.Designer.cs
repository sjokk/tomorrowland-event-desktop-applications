﻿namespace LoanableProductsReal
{
    partial class loanableProductForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.borrowerTb = new System.Windows.Forms.TextBox();
            this.idTicketTb = new System.Windows.Forms.TextBox();
            this.nameTicketTb = new System.Windows.Forms.TextBox();
            this.confirmBtn = new System.Windows.Forms.Button();
            this.lendOutRbtn = new System.Windows.Forms.RadioButton();
            this.returnRbtn = new System.Windows.Forms.RadioButton();
            this.borrowerIdLb = new System.Windows.Forms.Label();
            this.ticketIdLb = new System.Windows.Forms.Label();
            this.visitorNameLb = new System.Windows.Forms.Label();
            this.standManagementBtn = new System.Windows.Forms.Button();
            this.loanableProductGb.SuspendLayout();
            this.overviewGb.SuspendLayout();
            this.SuspendLayout();
            // 
            // loanableProductGb
            // 
            this.loanableProductGb.Controls.Add(this.borrowerIdLb);
            this.loanableProductGb.Controls.Add(this.borrowerTb);
            this.loanableProductGb.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.loanableProductGb.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.loanableProductGb.Size = new System.Drawing.Size(348, 274);
            this.loanableProductGb.Text = "Loanable Product Information";
            this.loanableProductGb.Controls.SetChildIndex(this.idLb, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.nameLb, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.priceLb, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.idTb, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.nameTb, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.priceTb, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.addProductBtn, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.borrowerTb, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.borrowerIdLb, 0);
            // 
            // overviewGb
            // 
            this.overviewGb.Controls.Add(this.idTicketTb);
            this.overviewGb.Controls.Add(this.nameTicketTb);
            this.overviewGb.Controls.Add(this.visitorNameLb);
            this.overviewGb.Controls.Add(this.ticketIdLb);
            this.overviewGb.Controls.Add(this.returnRbtn);
            this.overviewGb.Controls.Add(this.lendOutRbtn);
            this.overviewGb.Controls.Add(this.confirmBtn);
            this.overviewGb.Location = new System.Drawing.Point(363, 17);
            this.overviewGb.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.overviewGb.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.overviewGb.Controls.SetChildIndex(this.productsOverviewLb, 0);
            this.overviewGb.Controls.SetChildIndex(this.confirmBtn, 0);
            this.overviewGb.Controls.SetChildIndex(this.lendOutRbtn, 0);
            this.overviewGb.Controls.SetChildIndex(this.returnRbtn, 0);
            this.overviewGb.Controls.SetChildIndex(this.ticketIdLb, 0);
            this.overviewGb.Controls.SetChildIndex(this.visitorNameLb, 0);
            this.overviewGb.Controls.SetChildIndex(this.nameTicketTb, 0);
            this.overviewGb.Controls.SetChildIndex(this.idTicketTb, 0);
            // 
            // productsOverviewLb
            // 
            this.productsOverviewLb.ItemHeight = 20;
            this.productsOverviewLb.Items.AddRange(new object[] {
            "(37, Laptop Charger, €50.00, None)"});
            this.productsOverviewLb.Margin = new System.Windows.Forms.Padding(2);
            this.productsOverviewLb.Size = new System.Drawing.Size(458, 344);
            // 
            // priceTb
            // 
            this.priceTb.Location = new System.Drawing.Point(117, 118);
            this.priceTb.Margin = new System.Windows.Forms.Padding(2);
            this.priceTb.Text = "€150.00";
            // 
            // nameTb
            // 
            this.nameTb.Location = new System.Drawing.Point(117, 82);
            this.nameTb.Margin = new System.Windows.Forms.Padding(2);
            this.nameTb.Text = "Camera";
            // 
            // idTb
            // 
            this.idTb.Location = new System.Drawing.Point(117, 45);
            this.idTb.Margin = new System.Windows.Forms.Padding(2);
            this.idTb.Text = "228";
            // 
            // addProductBtn
            // 
            this.addProductBtn.Location = new System.Drawing.Point(6, 220);
            this.addProductBtn.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.addProductBtn.Size = new System.Drawing.Size(332, 34);
            this.addProductBtn.Click += new System.EventHandler(this.addProductBtn_Click);
            // 
            // removeBtn
            // 
            this.removeBtn.Location = new System.Drawing.Point(16, 323);
            this.removeBtn.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.removeBtn.Size = new System.Drawing.Size(330, 34);
            // 
            // borrowerTb
            // 
            this.borrowerTb.Location = new System.Drawing.Point(118, 154);
            this.borrowerTb.Name = "borrowerTb";
            this.borrowerTb.ReadOnly = true;
            this.borrowerTb.Size = new System.Drawing.Size(218, 26);
            this.borrowerTb.TabIndex = 7;
            this.borrowerTb.Text = "None";
            // 
            // idTicketTb
            // 
            this.idTicketTb.Location = new System.Drawing.Point(124, 43);
            this.idTicketTb.Name = "idTicketTb";
            this.idTicketTb.Size = new System.Drawing.Size(343, 26);
            this.idTicketTb.TabIndex = 8;
            this.idTicketTb.Text = "423145";
            // 
            // nameTicketTb
            // 
            this.nameTicketTb.Location = new System.Drawing.Point(124, 80);
            this.nameTicketTb.Name = "nameTicketTb";
            this.nameTicketTb.ReadOnly = true;
            this.nameTicketTb.Size = new System.Drawing.Size(343, 26);
            this.nameTicketTb.TabIndex = 9;
            this.nameTicketTb.Text = "Bardeby Jones";
            // 
            // confirmBtn
            // 
            this.confirmBtn.Location = new System.Drawing.Point(12, 558);
            this.confirmBtn.Name = "confirmBtn";
            this.confirmBtn.Size = new System.Drawing.Size(458, 42);
            this.confirmBtn.TabIndex = 5;
            this.confirmBtn.Text = "Lend Out Products";
            this.confirmBtn.UseVisualStyleBackColor = true;
            // 
            // lendOutRbtn
            // 
            this.lendOutRbtn.AutoSize = true;
            this.lendOutRbtn.Checked = true;
            this.lendOutRbtn.Location = new System.Drawing.Point(12, 505);
            this.lendOutRbtn.Name = "lendOutRbtn";
            this.lendOutRbtn.Size = new System.Drawing.Size(100, 24);
            this.lendOutRbtn.TabIndex = 10;
            this.lendOutRbtn.TabStop = true;
            this.lendOutRbtn.Text = "Lend Out";
            this.lendOutRbtn.UseVisualStyleBackColor = true;
            this.lendOutRbtn.CheckedChanged += new System.EventHandler(this.lendOutRbtn_CheckedChanged);
            // 
            // returnRbtn
            // 
            this.returnRbtn.AutoSize = true;
            this.returnRbtn.Location = new System.Drawing.Point(384, 505);
            this.returnRbtn.Name = "returnRbtn";
            this.returnRbtn.Size = new System.Drawing.Size(83, 24);
            this.returnRbtn.TabIndex = 11;
            this.returnRbtn.Text = "Return";
            this.returnRbtn.UseVisualStyleBackColor = true;
            this.returnRbtn.CheckedChanged += new System.EventHandler(this.returnRbtn_CheckedChanged);
            // 
            // borrowerIdLb
            // 
            this.borrowerIdLb.AutoSize = true;
            this.borrowerIdLb.Location = new System.Drawing.Point(8, 165);
            this.borrowerIdLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.borrowerIdLb.Name = "borrowerIdLb";
            this.borrowerIdLb.Size = new System.Drawing.Size(102, 20);
            this.borrowerIdLb.TabIndex = 10;
            this.borrowerIdLb.Text = "Borrower ID: ";
            // 
            // ticketIdLb
            // 
            this.ticketIdLb.AutoSize = true;
            this.ticketIdLb.Location = new System.Drawing.Point(8, 48);
            this.ticketIdLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ticketIdLb.Name = "ticketIdLb";
            this.ticketIdLb.Size = new System.Drawing.Size(76, 20);
            this.ticketIdLb.TabIndex = 12;
            this.ticketIdLb.Text = "Ticket ID:";
            // 
            // visitorNameLb
            // 
            this.visitorNameLb.AutoSize = true;
            this.visitorNameLb.Location = new System.Drawing.Point(8, 90);
            this.visitorNameLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.visitorNameLb.Name = "visitorNameLb";
            this.visitorNameLb.Size = new System.Drawing.Size(118, 20);
            this.visitorNameLb.TabIndex = 13;
            this.visitorNameLb.Text = "Visitor\'s Name: ";
            // 
            // standManagementBtn
            // 
            this.standManagementBtn.Location = new System.Drawing.Point(15, 575);
            this.standManagementBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.standManagementBtn.Name = "standManagementBtn";
            this.standManagementBtn.Size = new System.Drawing.Size(327, 42);
            this.standManagementBtn.TabIndex = 8;
            this.standManagementBtn.Text = "Stand Management";
            this.standManagementBtn.UseVisualStyleBackColor = true;
            this.standManagementBtn.Click += new System.EventHandler(this.standManagementBtn_Click);
            // 
            // loanableProductForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 637);
            this.Controls.Add(this.standManagementBtn);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(385, 348);
            this.Name = "loanableProductForm";
            this.Text = "Loanable Products Application";
            this.Controls.SetChildIndex(this.loanableProductGb, 0);
            this.Controls.SetChildIndex(this.overviewGb, 0);
            this.Controls.SetChildIndex(this.removeBtn, 0);
            this.Controls.SetChildIndex(this.standManagementBtn, 0);
            this.loanableProductGb.ResumeLayout(false);
            this.loanableProductGb.PerformLayout();
            this.overviewGb.ResumeLayout(false);
            this.overviewGb.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox borrowerTb;
        private System.Windows.Forms.TextBox nameTicketTb;
        private System.Windows.Forms.TextBox idTicketTb;
        private System.Windows.Forms.RadioButton returnRbtn;
        private System.Windows.Forms.RadioButton lendOutRbtn;
        private System.Windows.Forms.Button confirmBtn;
        private System.Windows.Forms.Label borrowerIdLb;
        private System.Windows.Forms.Label visitorNameLb;
        private System.Windows.Forms.Label ticketIdLb;
        private System.Windows.Forms.Button standManagementBtn;
    }
}

