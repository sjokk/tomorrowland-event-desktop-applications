﻿namespace LoanableProductsReal
{
    partial class LoanableProductsStandManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.productInsertionGbx.SuspendLayout();
            this.applicationPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // productInsertionGbx
            // 
            this.productInsertionGbx.Location = new System.Drawing.Point(12, 21);
            this.productInsertionGbx.Size = new System.Drawing.Size(389, 223);
            // 
            // productOverview
            // 
            this.productOverview.ItemHeight = 20;
            this.productOverview.Items.AddRange(new object[] {
            "(ProductID, Name, Price, Borrower)",
            "---------------------------------------"});
            this.productOverview.Location = new System.Drawing.Point(17, 269);
            this.productOverview.Margin = new System.Windows.Forms.Padding(2);
            this.productOverview.Size = new System.Drawing.Size(637, 324);
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(411, 46);
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // updateBtn
            // 
            this.updateBtn.Location = new System.Drawing.Point(411, 160);
            this.updateBtn.Size = new System.Drawing.Size(243, 44);
            this.updateBtn.Click += new System.EventHandler(this.updateBtn_Click);
            // 
            // removeBtn
            // 
            this.removeBtn.Location = new System.Drawing.Point(411, 101);
            this.removeBtn.Click += new System.EventHandler(this.removeBtn_Click);
            // 
            // stockTb
            // 
            this.stockTb.Margin = new System.Windows.Forms.Padding(2);
            // 
            // priceTb
            // 
            this.priceTb.Margin = new System.Windows.Forms.Padding(2);
            // 
            // nameTb
            // 
            this.nameTb.Location = new System.Drawing.Point(150, 92);
            this.nameTb.Margin = new System.Windows.Forms.Padding(2);
            // 
            // idTb
            // 
            this.idTb.Location = new System.Drawing.Point(150, 44);
            this.idTb.Margin = new System.Windows.Forms.Padding(2);
            this.idTb.ReadOnly = false;
            // 
            // stockLb
            // 
            this.stockLb.Location = new System.Drawing.Point(8, 181);
            // 
            // applicationPanel
            // 
            this.applicationPanel.BackColor = System.Drawing.Color.Transparent;
            this.applicationPanel.Size = new System.Drawing.Size(673, 607);
            // 
            // LoanableProductsStandManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 624);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "LoanableProductsStandManagement";
            this.Text = "Loanable Product Stand Management Application";
            this.productInsertionGbx.ResumeLayout(false);
            this.productInsertionGbx.PerformLayout();
            this.applicationPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
    }
}