﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StandNItems;

namespace ConsumableProducts
{
    public partial class LogIn : Form
    {
        private standManagement standManagement;
        public ConsumableStand consumableStand;

        //public LogIn()
        //{
        //    InitializeComponent();
        //    pwdTb.PasswordChar = '*';
        //}

        public LogIn(ConsumableStand consumableStand)
        {
            InitializeComponent();
            pwdTb.PasswordChar = '*';
            this.consumableStand = consumableStand;
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void loginBtn_Click(object sender, EventArgs e)
        {
            string password = pwdTb.Text;
            pwdTb.Clear();

            if (PWDDatabaseMethods.VerifyLogin(password))
            {
                // Construct a new standmanagement form.
                if (standManagement != null)
                {
                    standManagement.Close();
                }
                Hide();

                standManagement = new standManagement(consumableStand, this);
                standManagement.Show();
            }
            else
            {
                MessageBox.Show("Incorrect password.");
            }
        }
    }
}
