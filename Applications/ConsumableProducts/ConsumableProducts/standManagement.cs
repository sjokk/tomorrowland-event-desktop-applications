﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using standManagement;
using StandNItems;

namespace ConsumableProducts
{
    public partial class standManagement : standManagementForm
    {
        // Management class for this particular consumable stand.
        private ConsumableStand consumableStand;

        // Management class for event management's inserted products.
        private EventStandManagement eventStandManagement;

        private LogIn loginForm;

        /// <summary>
        /// Initialization of the containee form, which updates the product's
        /// in database overview every time it's opened and keeps track of changes
        /// made to the management class ConsumableStand which is further described
        /// in the summary of the parameter.
        /// </summary>
        /// <param name="consumableStand">
        /// The container form passes an object (management class) of ConsumableStand
        /// to the containee form which will then manipulate the object's information
        /// by adding products to the stand, updating product information etc.
        /// </param>
        public standManagement(ConsumableStand consumableStand, LogIn loginForm)
        {
            InitializeComponent();

            // Assign the main form's consumable stand management class to a private field of this form.
            this.consumableStand = consumableStand;
            this.loginForm = loginForm;

            if (consumableStand != null)
            {
                // Update the text of the containee form.
                Text = $"{this.consumableStand.StandId}: {this.consumableStand.Name} Management Application";

                // Initialize event management class.
                InitializeEventManagementProducts();

                // Update all listboxes.
                UpdateListBoxLines();
            }
            else
            {
                MessageBox.Show("Please ensure your VPN connection is established.");
                Application.Exit();
            }
        }

        /// <summary>
        /// Creates a consumable product object based on information provided in the four 
        /// textboxes.
        /// 
        /// Attempts to add the created object to the consumable stand management class
        /// and indicates whether this succeeded or not.
        /// 
        /// If it succeeded, the information is cleared from the textboxes and the list
        /// box is updated that represents the stand's inventory.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addBtn_Click(object sender, EventArgs e)
        {
            if (idTb.Text != string.Empty)
            {
                int productId = Convert.ToInt32(idTb.Text);
                string productName = nameTb.Text;

                if (productName != string.Empty)
                {
                    if (priceTb.Text != string.Empty)
                    {
                        decimal productPrice = Convert.ToDecimal(priceTb.Text);

                        if (stockTb.Text != string.Empty)
                        {
                            int quantity = Convert.ToInt32(stockTb.Text);

                            ConsumableProduct cp = new ConsumableProduct(productId, productName, productPrice, quantity);

                            bool isAdded = consumableStand.AddProduct(cp);

                            // Product id already exists in the management class, user gets appropriate message.
                            if (!isAdded)
                            {
                                MessageBox.Show($"Could not add product: {productName} as item already exists in the stand. Please use the update method or select a different product.");
                            }
                            else
                            {
                                // Write the information of the added product to database.
                                CPDatabaseMethods.AddProduct(consumableStand.StandId, productId, productPrice, quantity);

                                // Only if it was succesful we want to clear the textboxes (UX).
                                consumableProductsForm.ClearTextBoxes(productInsertionGbx);
                                // Refresh the stand's inventory list box.
                                UpdateProductsOverview();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Please provide a value for Quantity in Stock.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please provide a value for the product's sale price.");
                    }
                }
                else
                {
                    MessageBox.Show("Please select an existing product from the product overview.");
                }
            }
            else
            {
                MessageBox.Show("Please provide a valid product id for the product you want to edit.");
            }
        }

        /// <summary>
        /// If an employee changes the selection of an item in
        /// the listbox used for products inserted by the event
        /// management, the textboxes get updated with values 
        /// corresponding to the selected Consumable product.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void productsInDb_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Could even be further generalized based on the focus of a listbox to pass
            // the selected listbox to a method and process them both the same.
            if (productsInDb.SelectedIndex >= 3)
            {
                Product selectedProduct = (Product)productsInDb.SelectedItem;

                UpdateAllTextBoxInformation(selectedProduct);
            }
        }

        /// <summary>
        /// If an employee changes the selection of an item in
        /// the stand's inventory list box, the textboxes get
        /// updated with values corresponding to the selected
        /// Consumable product.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void productOverview_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (productOverview.SelectedIndex >= 3)
            {
                ConsumableProduct selectedCp = (ConsumableProduct)productOverview.SelectedItem;

                UpdateAllTextBoxInformation(selectedCp);
            }
        }

        /// <summary>
        /// Method that adds all the products that the event management
        /// inserted into the database to an event management class.
        /// </summary>
        private void InitializeEventManagementProducts()
        {
            // By application logic, there will always be inserted products first
            // by management, meaning this eventStandManagement class should always
            // and can always be initialized.
            eventStandManagement = new EventStandManagement();

            // Method in the DB child class will return a list with items as (id, prodName)
            List<Product> products = CPDatabaseMethods.GetConsumableProducts();

            foreach (Product p in products)
            {
                eventStandManagement.AddProduct(p);
            }
        }

        /// <summary>
        /// Method that updates all the textboxes with product information.
        /// </summary>
        /// <param name="p">
        /// A product object that will be used to polymorphically 
        /// update the textbox fields' either two or all of them
        /// based on the product's type.
        /// </param>
        private void UpdateAllTextBoxInformation(Product p)
        {
            // Clear all fields
            consumableProductsForm.ClearTextBoxes(productInsertionGbx);

            // Update first two fields.
            idTb.Text = p.Id.ToString();
            nameTb.Text = p.Name;

            // Update last two fields if product is of type ConsumableProduct.
            if (p is ConsumableProduct)
            {
                priceTb.Text = ((ConsumableProduct)p).Price.ToString();
                stockTb.Text = ((ConsumableProduct)p).NumberInStock.ToString();
            }
        }

        /// <summary>
        /// Calls two helper methods used to update all the information
        /// of both the stand's inventory listbox as well as the list
        /// box that holds the product names which were inserted into 
        /// the database by the event management.
        /// </summary>
        private void UpdateListBoxLines()
        {
            UpdateProductsOverview();
            UpdateProductsInDBOverView();
        }

        /// <summary>
        /// Method that updates the list box that is meant for providing
        /// an overview of this particular stand's inventory (Consumable Products in stock)
        /// 
        /// Method will provide three standard lines to the listbox, containing column
        /// headers and a seperator, followed by all of the products this stand's 
        /// management class holds.
        /// </summary>
        private void UpdateProductsOverview()
        {
            productOverview.Items.Clear();

            productOverview.Items.Add("Product name, price, quantity in stock");
            productOverview.Items.Add("--------------------------------------");
            productOverview.Items.Add("");

            foreach (Product p in consumableStand.GetProducts())
            {
                productOverview.Items.Add(p);
            }
        }

        /// <summary>
        /// Method that updates the list box that provides consumable products'
        /// as inserted by the event management.
        /// 
        /// Method will provide three standard lines to the listbox, containing
        /// column headers and a seperator, followed by all of the products
        /// that exist in the management class.
        /// </summary>
        private void UpdateProductsInDBOverView()
        {
            productsInDb.Items.Add("Product id, product name");
            productsInDb.Items.Add("--------------------------------------");
            productsInDb.Items.Add("");

            foreach (Product p in eventStandManagement.GetProducts())
            {
                productsInDb.Items.Add(p);
            }
        }

        private void idTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = consumableProductsForm.ValidateInput(e);
        }

        private void stockTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = consumableProductsForm.ValidateInput(e);
        }

        private void priceTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
            if ((consumableProductsForm.ValidateInput(e) == false) || (consumableProductsForm.ValidateDecimalInput(e) == false))
            {
                e.Handled = false;
            }
        }

        private void idTb_TextChanged(object sender, EventArgs e)
        {
            // Obtain the value in the idTb
            // If this value is not an empty string, we process
            if (idTb.Text != string.Empty)
            {
                int productId; 
                bool result = int.TryParse(idTb.Text, out productId);

                if (result)
                {
                    // Obtain product
                    Product selectedP = eventStandManagement.GetProduct(productId);

                    // if prod not null update fields. 
                    if (selectedP != null)
                    {
                        nameTb.Text = selectedP.Name;
                    }
                    else
                    {
                        consumableProductsForm.ClearAllButId(productInsertionGbx);
                    }
                }
            }
            else
            {
                consumableProductsForm.ClearTextBoxes(productInsertionGbx);
            }
        }

        /// <summary>
        /// Method that tries to update the price and number in stock for a selected
        /// consumable product.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateBtn_Click(object sender, EventArgs e)
        {
            if (idTb.Text != string.Empty)
            {
                int productId = Convert.ToInt32(idTb.Text);

                ConsumableProduct selectedCp = (ConsumableProduct)consumableStand.GetProduct(productId);

                if (selectedCp != null)
                {
                    if ((priceTb.Text != string.Empty) && (stockTb.Text != string.Empty))
                    {
                        selectedCp.Price = Convert.ToDecimal(priceTb.Text);
                        selectedCp.NumberInStock = Convert.ToInt32(stockTb.Text);

                        CPDatabaseMethods.UpdateProduct(consumableStand.StandId, productId, selectedCp.Price, selectedCp.NumberInStock);

                        UpdateProductsOverview();
                        consumableProductsForm.ClearTextBoxes(productInsertionGbx);
                    }
                    else
                    {
                        MessageBox.Show("Please provide a value for both Price and Quantity in Stock before trying to update.");
                    }
                }
                else
                {
                    MessageBox.Show($"Could not obtain a product for id: {productId}");
                }
            }
            else
            {
                MessageBox.Show("Please enter a valid product ID before trying to update.");
            }
        }

        /// <summary>
        /// Removes the selected product from the management class
        /// as well as flags the stand, product combination as 
        /// removed in the database.
        /// 
        /// Resulting in the item not being loaded whenever the 
        /// application is launched a next time.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void removeBtn_Click(object sender, EventArgs e)
        {
            // Flag product as removed and remove product by id from consumable stand class.
            if (idTb.Text != string.Empty)
            {
                int productId = Convert.ToInt32(idTb.Text);

                ConsumableProduct selectedCp = (ConsumableProduct)consumableStand.GetProduct(productId);
                if (selectedCp != null)
                {
                    consumableStand.RemoveProduct(productId);
                    CPDatabaseMethods.RemoveProduct(consumableStand.StandId, productId);

                    consumableProductsForm.ClearTextBoxes(productInsertionGbx);
                    UpdateProductsOverview();
                }
                else
                {
                    MessageBox.Show($"Could not obtain a product for product id: {productId}");
                }
            }
            else
            {
                MessageBox.Show("Please select a product or enter an ID for the product you want to remove.");
            }
        }

        private void standManagement_FormClosing(object sender, FormClosingEventArgs e)
        {
            loginForm.Close();
        }
    }
}
