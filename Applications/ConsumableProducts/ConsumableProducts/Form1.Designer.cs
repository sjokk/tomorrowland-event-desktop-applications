﻿namespace ConsumableProducts
{
    partial class consumableProductsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.idTicketTb = new System.Windows.Forms.TextBox();
            this.totalTb = new System.Windows.Forms.TextBox();
            this.totalLb = new System.Windows.Forms.Label();
            this.confirmBtn = new System.Windows.Forms.Button();
            this.idTicketLb = new System.Windows.Forms.Label();
            this.accountBalanceLb = new System.Windows.Forms.Label();
            this.accountBalanceTb = new System.Windows.Forms.TextBox();
            this.qtyLb = new System.Windows.Forms.Label();
            this.standManagementBtn = new System.Windows.Forms.Button();
            this.confirmNameBtn = new System.Windows.Forms.Button();
            this.standNameTb = new System.Windows.Forms.TextBox();
            this.standNameLb = new System.Windows.Forms.Label();
            this.standInfoGrpbx = new System.Windows.Forms.GroupBox();
            this.qtyNumUpDown = new System.Windows.Forms.NumericUpDown();
            this.loanableProductGb.SuspendLayout();
            this.overviewGb.SuspendLayout();
            this.standInfoGrpbx.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qtyNumUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // loanableProductGb
            // 
            this.loanableProductGb.Controls.Add(this.qtyNumUpDown);
            this.loanableProductGb.Controls.Add(this.qtyLb);
            this.loanableProductGb.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.loanableProductGb.Padding = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.loanableProductGb.Size = new System.Drawing.Size(229, 202);
            this.loanableProductGb.Text = "Consumable Products Information";
            this.loanableProductGb.Controls.SetChildIndex(this.qtyLb, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.idLb, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.nameLb, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.priceLb, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.idTb, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.nameTb, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.priceTb, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.addProductBtn, 0);
            this.loanableProductGb.Controls.SetChildIndex(this.qtyNumUpDown, 0);
            // 
            // overviewGb
            // 
            this.overviewGb.Controls.Add(this.accountBalanceTb);
            this.overviewGb.Controls.Add(this.accountBalanceLb);
            this.overviewGb.Controls.Add(this.idTicketLb);
            this.overviewGb.Controls.Add(this.confirmBtn);
            this.overviewGb.Controls.Add(this.totalLb);
            this.overviewGb.Controls.Add(this.idTicketTb);
            this.overviewGb.Controls.Add(this.totalTb);
            this.overviewGb.Location = new System.Drawing.Point(238, 6);
            this.overviewGb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.overviewGb.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.overviewGb.Size = new System.Drawing.Size(324, 380);
            this.overviewGb.Text = "Receipt Overview";
            this.overviewGb.Controls.SetChildIndex(this.productsOverviewLb, 0);
            this.overviewGb.Controls.SetChildIndex(this.totalTb, 0);
            this.overviewGb.Controls.SetChildIndex(this.idTicketTb, 0);
            this.overviewGb.Controls.SetChildIndex(this.totalLb, 0);
            this.overviewGb.Controls.SetChildIndex(this.confirmBtn, 0);
            this.overviewGb.Controls.SetChildIndex(this.idTicketLb, 0);
            this.overviewGb.Controls.SetChildIndex(this.accountBalanceLb, 0);
            this.overviewGb.Controls.SetChildIndex(this.accountBalanceTb, 0);
            // 
            // productsOverviewLb
            // 
            this.productsOverviewLb.Location = new System.Drawing.Point(8, 80);
            this.productsOverviewLb.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.productsOverviewLb.Size = new System.Drawing.Size(307, 238);
            // 
            // priceTb
            // 
            this.priceTb.Enabled = false;
            this.priceTb.Location = new System.Drawing.Point(62, 77);
            this.priceTb.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.priceTb.Size = new System.Drawing.Size(164, 20);
            this.priceTb.Text = "";
            // 
            // nameTb
            // 
            this.nameTb.Enabled = false;
            this.nameTb.Location = new System.Drawing.Point(62, 53);
            this.nameTb.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.nameTb.Size = new System.Drawing.Size(164, 20);
            this.nameTb.Text = "";
            // 
            // idTb
            // 
            this.idTb.Enabled = false;
            this.idTb.Location = new System.Drawing.Point(62, 29);
            this.idTb.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.idTb.Size = new System.Drawing.Size(164, 20);
            this.idTb.Text = "";
            this.idTb.TextChanged += new System.EventHandler(this.idTb_TextChanged);
            this.idTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.idTb_KeyPress);
            // 
            // addProductBtn
            // 
            this.addProductBtn.Location = new System.Drawing.Point(5, 159);
            this.addProductBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.addProductBtn.Size = new System.Drawing.Size(218, 22);
            this.addProductBtn.Click += new System.EventHandler(this.addProductBtn_Click);
            // 
            // removeBtn
            // 
            this.removeBtn.Location = new System.Drawing.Point(11, 256);
            this.removeBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.removeBtn.Size = new System.Drawing.Size(218, 22);
            this.removeBtn.Click += new System.EventHandler(this.removeBtn_Click);
            // 
            // idTicketTb
            // 
            this.idTicketTb.Enabled = false;
            this.idTicketTb.Location = new System.Drawing.Point(102, 16);
            this.idTicketTb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.idTicketTb.Name = "idTicketTb";
            this.idTicketTb.Size = new System.Drawing.Size(214, 20);
            this.idTicketTb.TabIndex = 5;
            this.idTicketTb.TextChanged += new System.EventHandler(this.idTicketTb_TextChanged);
            this.idTicketTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.idTicketTb_KeyPress);
            // 
            // totalTb
            // 
            this.totalTb.Enabled = false;
            this.totalTb.Location = new System.Drawing.Point(76, 324);
            this.totalTb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.totalTb.Name = "totalTb";
            this.totalTb.ReadOnly = true;
            this.totalTb.Size = new System.Drawing.Size(166, 20);
            this.totalTb.TabIndex = 6;
            // 
            // totalLb
            // 
            this.totalLb.AutoSize = true;
            this.totalLb.Location = new System.Drawing.Point(8, 328);
            this.totalLb.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.totalLb.Name = "totalLb";
            this.totalLb.Size = new System.Drawing.Size(64, 13);
            this.totalLb.TabIndex = 7;
            this.totalLb.Text = "Total Price: ";
            // 
            // confirmBtn
            // 
            this.confirmBtn.Location = new System.Drawing.Point(8, 348);
            this.confirmBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.confirmBtn.Name = "confirmBtn";
            this.confirmBtn.Size = new System.Drawing.Size(305, 23);
            this.confirmBtn.TabIndex = 5;
            this.confirmBtn.Text = "Confirm Purchase";
            this.confirmBtn.UseVisualStyleBackColor = true;
            this.confirmBtn.Click += new System.EventHandler(this.confirmBtn_Click);
            // 
            // idTicketLb
            // 
            this.idTicketLb.AutoSize = true;
            this.idTicketLb.Location = new System.Drawing.Point(8, 23);
            this.idTicketLb.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.idTicketLb.Name = "idTicketLb";
            this.idTicketLb.Size = new System.Drawing.Size(54, 13);
            this.idTicketLb.TabIndex = 8;
            this.idTicketLb.Text = "Ticket ID:";
            // 
            // accountBalanceLb
            // 
            this.accountBalanceLb.AutoSize = true;
            this.accountBalanceLb.Location = new System.Drawing.Point(8, 49);
            this.accountBalanceLb.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.accountBalanceLb.Name = "accountBalanceLb";
            this.accountBalanceLb.Size = new System.Drawing.Size(92, 13);
            this.accountBalanceLb.TabIndex = 9;
            this.accountBalanceLb.Text = "Account Balance:";
            // 
            // accountBalanceTb
            // 
            this.accountBalanceTb.Enabled = false;
            this.accountBalanceTb.Location = new System.Drawing.Point(102, 42);
            this.accountBalanceTb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.accountBalanceTb.Name = "accountBalanceTb";
            this.accountBalanceTb.ReadOnly = true;
            this.accountBalanceTb.Size = new System.Drawing.Size(214, 20);
            this.accountBalanceTb.TabIndex = 10;
            // 
            // qtyLb
            // 
            this.qtyLb.AutoSize = true;
            this.qtyLb.Location = new System.Drawing.Point(6, 107);
            this.qtyLb.Name = "qtyLb";
            this.qtyLb.Size = new System.Drawing.Size(52, 13);
            this.qtyLb.TabIndex = 8;
            this.qtyLb.Text = "Quantity: ";
            // 
            // standManagementBtn
            // 
            this.standManagementBtn.Location = new System.Drawing.Point(12, 354);
            this.standManagementBtn.Name = "standManagementBtn";
            this.standManagementBtn.Size = new System.Drawing.Size(217, 23);
            this.standManagementBtn.TabIndex = 8;
            this.standManagementBtn.Text = "Stand Management";
            this.standManagementBtn.UseVisualStyleBackColor = true;
            this.standManagementBtn.Visible = false;
            this.standManagementBtn.Click += new System.EventHandler(this.standManagementBtn_Click);
            // 
            // confirmNameBtn
            // 
            this.confirmNameBtn.Location = new System.Drawing.Point(16, 56);
            this.confirmNameBtn.Name = "confirmNameBtn";
            this.confirmNameBtn.Size = new System.Drawing.Size(185, 23);
            this.confirmNameBtn.TabIndex = 12;
            this.confirmNameBtn.Text = "Set Name";
            this.confirmNameBtn.UseVisualStyleBackColor = true;
            this.confirmNameBtn.Click += new System.EventHandler(this.confirmNameBtn_Click);
            // 
            // standNameTb
            // 
            this.standNameTb.Location = new System.Drawing.Point(101, 24);
            this.standNameTb.Name = "standNameTb";
            this.standNameTb.Size = new System.Drawing.Size(100, 20);
            this.standNameTb.TabIndex = 11;
            // 
            // standNameLb
            // 
            this.standNameLb.AutoSize = true;
            this.standNameLb.Location = new System.Drawing.Point(13, 31);
            this.standNameLb.Name = "standNameLb";
            this.standNameLb.Size = new System.Drawing.Size(82, 13);
            this.standNameLb.TabIndex = 10;
            this.standNameLb.Text = "Name of stand: ";
            // 
            // standInfoGrpbx
            // 
            this.standInfoGrpbx.Controls.Add(this.confirmNameBtn);
            this.standInfoGrpbx.Controls.Add(this.standNameLb);
            this.standInfoGrpbx.Controls.Add(this.standNameTb);
            this.standInfoGrpbx.Location = new System.Drawing.Point(12, 403);
            this.standInfoGrpbx.Name = "standInfoGrpbx";
            this.standInfoGrpbx.Size = new System.Drawing.Size(217, 100);
            this.standInfoGrpbx.TabIndex = 13;
            this.standInfoGrpbx.TabStop = false;
            this.standInfoGrpbx.Text = "Stand Information";
            // 
            // qtyNumUpDown
            // 
            this.qtyNumUpDown.Enabled = false;
            this.qtyNumUpDown.Location = new System.Drawing.Point(62, 100);
            this.qtyNumUpDown.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.qtyNumUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.qtyNumUpDown.Name = "qtyNumUpDown";
            this.qtyNumUpDown.Size = new System.Drawing.Size(164, 20);
            this.qtyNumUpDown.TabIndex = 10;
            this.qtyNumUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // consumableProductsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 513);
            this.Controls.Add(this.standInfoGrpbx);
            this.Controls.Add(this.standManagementBtn);
            this.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.MinimumSize = new System.Drawing.Size(0, 0);
            this.Name = "consumableProductsForm";
            this.Text = "Consumable Products Application";
            this.Controls.SetChildIndex(this.standManagementBtn, 0);
            this.Controls.SetChildIndex(this.loanableProductGb, 0);
            this.Controls.SetChildIndex(this.overviewGb, 0);
            this.Controls.SetChildIndex(this.removeBtn, 0);
            this.Controls.SetChildIndex(this.standInfoGrpbx, 0);
            this.loanableProductGb.ResumeLayout(false);
            this.loanableProductGb.PerformLayout();
            this.overviewGb.ResumeLayout(false);
            this.overviewGb.PerformLayout();
            this.standInfoGrpbx.ResumeLayout(false);
            this.standInfoGrpbx.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qtyNumUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label totalLb;
        private System.Windows.Forms.TextBox idTicketTb;
        private System.Windows.Forms.TextBox totalTb;
        private System.Windows.Forms.TextBox accountBalanceTb;
        private System.Windows.Forms.Label accountBalanceLb;
        private System.Windows.Forms.Label idTicketLb;
        private System.Windows.Forms.Button confirmBtn;
        private System.Windows.Forms.Label qtyLb;
        private System.Windows.Forms.Button standManagementBtn;
        private System.Windows.Forms.Button confirmNameBtn;
        private System.Windows.Forms.TextBox standNameTb;
        private System.Windows.Forms.Label standNameLb;
        private System.Windows.Forms.GroupBox standInfoGrpbx;
        private System.Windows.Forms.NumericUpDown qtyNumUpDown;
    }
}

