﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;
using LoanableProducts;
using StandNItems;
using Phidget22;
using Phidget22.Events;
using System.Drawing;
using AForge;
using AForge.Video;
using AForge.Video.DirectShow;
using ZXing;

namespace ConsumableProducts
{
    public partial class consumableProductsForm : productsForm
    {
        // Muh scanner
        private RFID myRFIDReader;

        // Management class for this particular consumable products stand.
        public ConsumableStand consumableStand;
        
        // Second form, used by the stand's employees for managing their particular stand's products.
        //private standManagement standManagement;
        private LogIn loginForm;

        // Class to keep track of the customer, if any was scanned or manually entered.
        Customer currentCustomer;

        // Management class to keep track of a sales order before it's being confirmed.
        Receipt currentReceipt;

        // RFID tag.
        string currentTag = null;

        public consumableProductsForm()
        {
            InitializeComponent();

            // Try to create a new muh scanner object and subscribe event handlers to the events.
            try
            {
                myRFIDReader = new RFID();
                myRFIDReader.Attach += isAttached;
                myRFIDReader.Detach += isDetached;
                myRFIDReader.Tag += MyRFIDReader_Tag;
                myRFIDReader.TagLost += MyRFIDReader_TagLost;

            }
            catch (PhidgetException ex)
            {
                MessageBox.Show(ex.Message);
            }

            // Check if the standId already exists in the config file
            // if it does, skip inserting a stand name and initialize
            // all stand information that exists in the database.
            if (ConfigurationManager.AppSettings["standId"] != null)
            {
                ResizeForm();
                InitializeStand();

                currentReceipt = new Receipt(consumableStand.StandId);
            }
        }

        private void isAttached(object sender, AttachEventArgs e)
        {
            MessageBox.Show("Scanner is attached");
        }

        private void isDetached(object sender, DetachEventArgs e)
        {
            MessageBox.Show("Scanner is detached");
        }

        private void MyRFIDReader_Tag(object sender, RFIDTagEventArgs e)
        {
            // Use this tag to query the db and obtain a relation with the ticketId.
            currentTag = e.Tag;
            int consumableProductId = -1;
            currentCustomer = CPDatabaseMethods.ObtainTicketByTag(currentTag, ref consumableProductId);

            if (currentCustomer != null)
            {
                // Success
                idTicketTb.BackColor = Color.Green;
                idTicketTb.Text = currentCustomer.TicketId.ToString();
            }
            else if (consumableProductId > 0)
            {
                // In case the customer object still refers to a null object,
                // the obtained ticketId by reference is meant for products.
                idTb.BackColor = Color.Green;
                idTb.Text = consumableProductId.ToString();
            }
            else
            {
                // Fail to obtain a customer for tag thus red color.
                idTicketTb.BackColor = Color.Red;
                idTicketTb.Text = "";
                accountBalanceTb.Text = "";
            }
        }
        private void MyRFIDReader_TagLost(object sender, RFIDTagLostEventArgs e)
        {
            idTicketTb.BackColor = this.BackColor;
            idTb.BackColor = this.BackColor;
            //currentCustomer = CPDatabaseMethods.ObtainTicketByTag(currentTag);

            //if (currentCustomer != null)
            //{
            //    idTicketTb.Text = currentCustomer.TicketId.ToString();
            //}
            //else
            //{
            //    // refresh the text boxes.
            //    idTicketTb.Text = "";
            //    accountBalanceTb.Text = "";
            //}
        }
        
        /// <summary>
        /// Closes any old stand management form if there exists one, 
        /// before re-opening a new stand management form (such that 
        /// only one concurrent stand management form is open). 
        /// 
        /// Also passes this stand management class as an argument to 
        /// the constructor of the containee form allowing the stand 
        /// management form to use several of the members of this form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void standManagementBtn_Click(object sender, EventArgs e)
        {
            //// Ensures only one form is opened at a time.
            //if (standManagement != null)
            //{
            //    standManagement.Close();
            //}
           
            //// Provide the stand's management class to the containee form
            //// such that the employee can manipulate the management class
            //// from within the respective application.
            //standManagement = new standManagement(consumableStand);
            //standManagement.Show();

            if (loginForm != null)
            {
                loginForm.Close();
            }
            loginForm = new LogIn(consumableStand);
            loginForm.Show();
        }

        /// <summary>
        /// Obtains the inserted name for the consumable product stand,
        /// inserts this name into the database while obtaining the unique
        /// id for the stand.
        /// 
        /// After obtaining the unique id, the consumable product stand is created 
        /// as an object and assigned to the form field (consumableStand).
        /// 
        /// Also saves the stand's ID into the application configuration file,
        /// which is then in turn used on the next strart up of the app
        /// to identify the stand and it's items (state).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void confirmNameBtn_Click(object sender, EventArgs e)
        {
            string standName = standNameTb.Text; ;
            int standId;

            if (standName == string.Empty)
            {
                MessageBox.Show("Please enter a name for the stand.");
                return;
            }
            else
            {
                // Call method to insert stand name and obtain the surrogate key (id).
                standId = CPDatabaseMethods.InsertStand(standName);

                // Create a consumable stand object.
                consumableStand = new ConsumableStand(standId, standName);

                // Create an initial receipt.
                currentReceipt = new Receipt(consumableStand.StandId);
            }
            // Update standid in the .exe config file (runtime).
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings.Add("standId", standId.ToString());
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            
            // Resize the form to hide textbox for stand name input.
            ResizeForm();
            EnableConsumableStandProducts();
        }

        /// <summary>
        /// Method that obtains the stand's id from the runtime app config file,
        /// using this stand id to obtain the stand's name from the database.
        /// 
        /// Further more, it also obtains all the already existing consumable
        /// products that were inserted for this stand and recreates them while
        /// adding those to the stand management class.
        /// </summary>
        private void InitializeStand()
        {
            // Obtain the stand's name via the id that is supplied in the config file.
            int standId = Convert.ToInt32(ConfigurationManager.AppSettings["standId"]);

            consumableStand = CPDatabaseMethods.GetConsumableProductStand(standId);

            List<ConsumableProduct> standsConsumableProducts = CPDatabaseMethods.GetStandConsumableProducts(standId);

            foreach (ConsumableProduct cp in standsConsumableProducts)
            {
                consumableStand.AddProduct(cp);
            }

            EnableConsumableStandProducts();
        }

        /// <summary>
        /// Helper method for resizing the form in case the name of 
        /// the stand has been inserted by an employee for the first
        /// time or the standId already exists in the app's config file.
        /// </summary>
        private void ResizeForm()
        {
            standInfoGrpbx.Visible = false;
            Height = 431;
            standManagementBtn.Visible = true;
        }

        /// <summary>
        /// Helper method to update the product's Name and
        /// price textbox based on the inserted consumable product.
        /// </summary>
        /// <param name="selectedCp"></param>
        private void UpdateProductInformation(ConsumableProduct selectedCp)
        {
            nameTb.Text = selectedCp.Name;
            priceTb.Text = selectedCp.Price.ToString();
            qtyNumUpDown.Value = 1;
        }

        /// <summary>
        /// Helper method that checks if an input keypress event is either
        /// a digit or a backspace character for integer only textboxes.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static bool ValidateInput(KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) || (e.KeyChar == (char)8))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Additional helper method, validates if input character is a dot.
        /// </summary>
        /// <param name="e"></param>
        /// <returns>
        /// Returns a boolean representing whether character is handled or not.
        /// </returns>
        public static bool ValidateDecimalInput(KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)46)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Whenever a consumable product stand management object is initialized
        /// the textboxes on the main form that provide functionality to this
        /// particular stand are enabled as well as functionality derrived from
        /// this particular stand.
        /// </summary>
        private void EnableConsumableStandProducts()
        {
            // Enable all textboxes of groupbox.
            EnableTextBoxes(loanableProductGb);
            EnableTextBoxes(overviewGb);

            // Enable numUpDown
            qtyNumUpDown.Enabled = true;

            // Open the RFID scanner for usage.
            try
            {
                myRFIDReader.Open();
            }
            catch (PhidgetException)
            {
                MessageBox.Show("Could not connect to the RFID-Reader!");
            }
        }

        /// <summary>
        /// Helper method that iterates over all the controls
        /// of a groupbox that is passed into the method as 
        /// arguement.
        /// </summary>
        /// <param name="gbx">
        /// gbx is a selected groupbox object for which the user
        /// wants to enable textboxes.
        /// </param>
        private void EnableTextBoxes(GroupBox gbx)
        {
            foreach (Control guiControl in gbx.Controls)
            {
                if (guiControl is TextBox)
                {
                    guiControl.Enabled = true;
                }
            }
        }

        /// <summary>
        /// Checks if the pressed key is a digit or a backspace character
        /// and sets the keypress to handled false if it does match aforementiond
        /// and to true otherwise.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void idTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidateInput(e);
        }

        /// <summary>
        /// Validates if the string in the idTb is not an empty string,
        /// if it is not, it tries to obtain a product for that id. If a
        /// product with such an ID exists, the Textboxes get updated with
        /// corresonding information.
        /// 
        /// Otherwise all the textboxes except for the textbox with id in it's
        /// name will be cleared from text data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void idTb_TextChanged(object sender, EventArgs e)
        {
            // Obtain the value in the idTb
            // If this value is not an empty string, we process
            if (idTb.Text != string.Empty)
            {
                int productId;

                bool result = int.TryParse(idTb.Text, out productId);

                if (result)
                {
                    // Obtain product
                    ConsumableProduct selectedCp = (ConsumableProduct)consumableStand.GetProduct(productId);

                    // if prod not null update fields. 
                    if (selectedCp != null)
                    {
                        UpdateProductInformation(selectedCp);
                    }
                    else
                    {
                        ClearAllButId(loanableProductGb);
                    }
                }
            }
            else
            {
                ClearTextBoxes(loanableProductGb);
            }
        }

        /// <summary>
        /// Clears all the groupbox' textboxes from its text.
        /// </summary>
        public static void ClearTextBoxes(GroupBox groupBox)
        {
            foreach (Control guiControl in groupBox.Controls)
            {
                if (guiControl is TextBox)
                {
                    ((TextBox)guiControl).Clear();
                }
            }
        }

        /// <summary>
        /// Clears all textboxes on the inserted groupbox that do
        /// not contain id in the text of their Name property.
        /// </summary>
        /// <param name="groupBox"></param>
        public static void ClearAllButId(GroupBox groupBox)
        {
            foreach (Control guiControl in groupBox.Controls)
            {
                if (guiControl is TextBox)
                {
                    if (!guiControl.Name.Contains("id"))
                    {
                        ((TextBox)guiControl).Clear();
                    }
                }
            }
        }

        /// <summary>
        /// Handles integer input for the ticket id textbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void idTicketTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidateInput(e);
        }

        /// <summary>
        /// Follows the same logic as the idTb for product id, except that
        /// for obtaining the balance of particular user, the database needs
        /// to be queried.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void idTicketTb_TextChanged(object sender, EventArgs e)
        {
            // If this value is not an empty string, we process
            if (idTicketTb.Text != string.Empty)
            {
                int ticketId;
                bool result = int.TryParse(idTicketTb.Text, out ticketId);

                if (result)
                {
                    // Obtain customer
                    currentCustomer = CPDatabaseMethods.GetCustomer(ticketId);

                    // if Customer is not null update fields. 
                    if (currentCustomer != null)
                    {
                        accountBalanceTb.Text = $"€{currentCustomer.Balance.ToString()}";
                    }
                }
            }
            else
            {
                // Clear Balance Textbox
                accountBalanceTb.Clear();
            }
        }

        /// <summary>
        /// Also only handles digits and backspaces as input.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void quantityTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidateInput(e);
        }

        /// <summary>
        /// Validates all input fields, if it passed all the validation,
        /// this event handler ensures the creation of a new receipt line
        /// that is then added to the receipt (management class).
        /// 
        /// Updates the corresponding list box to provide an up to date overview.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addProductBtn_Click(object sender, EventArgs e)
        {
            if (idTb.Text != string.Empty)
            {
                int productId = Convert.ToInt32(idTb.Text);
                ConsumableProduct cp = (ConsumableProduct)consumableStand.GetProduct(productId);

                if (cp != null)
                {
                    if (qtyNumUpDown.Value > 0)
                    {
                        int quantity = Convert.ToInt32(qtyNumUpDown.Value);

                        if (qtyNumUpDown.Value <= cp.NumberInStock)
                        {
                            int salesQty = Convert.ToInt32(qtyNumUpDown.Value);
                            
                            currentReceipt.AddReceiptLine(new ReceiptLine(productId, cp.Name, cp.Price, salesQty));

                            UpdateReceiptListBox();
                        }
                        else
                        {
                            MessageBox.Show($"Only {cp.NumberInStock} products in stock, your quantity entered: {qtyNumUpDown.Value}.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please enter a quantity > 0.");
                    }
                }
                else
                {
                    MessageBox.Show($"Could not obtain a product for product ID: {productId}");
                }
            }
            else
            {
                MessageBox.Show("Please enter a product ID.");
            }
        }

        /// <summary>
        /// Helper method, clears the corresponding list box overview,
        /// re-adds the standard attribute headers and adds all the
        /// receipt lines in the current receipt to the overview.
        /// 
        /// Also updates the receipt total textbox, showing the 
        /// total costs of current order.
        /// </summary>
        private void UpdateReceiptListBox()
        {
            productsOverviewLb.Items.Clear();
            AddBasicReceiptColumnHeaders();

            foreach (ReceiptLine rl in currentReceipt.GetReceiptLines())
            {
                productsOverviewLb.Items.Add(rl);
            }

            totalTb.Text = "€" + currentReceipt.ReceiptTotal().ToString();
        }

        /// <summary>
        /// Provides the list box overview that holds the receipt lines of receipt
        /// with standard attribute headers, providing an indication of what the values
        /// in the receipt line mean.
        /// </summary>
        private void AddBasicReceiptColumnHeaders()
        {
            productsOverviewLb.Items.Add("(Product id, Product name, Sales price, Sales quantity, Subtotal)");
            productsOverviewLb.Items.Add("--------------------------------------");
            productsOverviewLb.Items.Add("");
        }

        /// <summary>
        /// Based on the user's selected index, this method obtains that receipt line
        /// from the list box, converts this to a receipt line object and tries to remove
        /// said receipt line from the receipt managmenet class' list of receipt lines.
        /// 
        /// After doing so, it updates the overview with most up to date information.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void removeBtn_Click(object sender, EventArgs e)
        {
            if (productsOverviewLb.SelectedIndex >= 3)
            {
                ReceiptLine selectedRl = (ReceiptLine)productsOverviewLb.SelectedItem; // (ReceiptLine)productsOverviewLb.Items[productsOverviewLb.SelectedIndex];

                currentReceipt.RemoveReceiptLine(selectedRl);

                UpdateReceiptListBox();
            }
            else
            {
                MessageBox.Show("Please select a product that you want to remove.");
            }
        }

        /// <summary>
        /// Method that validates if all information to confirm the order is provided
        /// and valid, if so, this method will finalize the order by manipulating
        /// all involved classes and process the data to the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void confirmBtn_Click(object sender, EventArgs e)
        {
            if (idTicketTb.Text != string.Empty)
            {
                if (currentCustomer != null)
                {
                    if (currentReceipt.GetReceiptLines().Count > 0)
                    {
                        if (currentCustomer.Balance >= currentReceipt.ReceiptTotal())
                        {
                            // We dodged all pit falls, transaction will be made definitive.

                            // Insert receipt's information also obtaining the id of the receipt from the db.
                            int receiptId = CPDatabaseMethods.InsertReceipt(currentCustomer.TicketId);

                            foreach (ReceiptLine rl in currentReceipt.GetReceiptLines())
                            {
                                CPDatabaseMethods.InsertReceiptLine(receiptId, consumableStand.StandId, rl);
                            }

                            currentCustomer.DecreaseBalance(currentReceipt.ReceiptTotal());
                            consumableStand.DeductInventory(currentReceipt);

                            // Update all product items that stand holds into the db such that stand object and db are in sync.
                            foreach (ConsumableProduct currentCp in consumableStand.GetProducts())
                            {
                                CPDatabaseMethods.UpdateProduct(consumableStand.StandId, currentCp.Id, currentCp.Price, currentCp.NumberInStock);
                            }

                            currentReceipt = new Receipt(consumableStand.StandId);
                            currentCustomer = null;

                            ClearTextBoxes(overviewGb);
                            ClearTextBoxes(loanableProductGb);
                            qtyNumUpDown.Value = 1;
                            productsOverviewLb.Items.Clear();
                        }
                        else
                        {
                            MessageBox.Show($"Customer's account balance too low: €{currentCustomer.Balance}, please delete a product or update balance at the ATM.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please add a product to the sales order.");
                    }
                }
                else
                {
                    MessageBox.Show($"No customer exists for id: {idTicketTb.Text}");
                }
            }
            else
            {
                MessageBox.Show("Please enter a ticket id.");
            }
        }
    }
}
