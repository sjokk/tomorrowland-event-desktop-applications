﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBaseMethods;
using MySql.Data.MySqlClient;

namespace ConsumableProducts
{
    class PWDDatabaseMethods : DataBase
    {
        public static bool VerifyLogin(string password)
        {
            string sql = "SELECT privilegePassword FROM stand_management_privileges WHERE idPrivilege = 1;";
            MySqlDataReader rdr = ExecuteReader(sql);
            string resultPassword = string.Empty;
            while (rdr.Read())
            {
                resultPassword = rdr["privilegePassword"].ToString();
            }
            CloseConnection();

            if (resultPassword != string.Empty && password == resultPassword)
            {
                return true;
            }
            return false;
        }
    }
}
