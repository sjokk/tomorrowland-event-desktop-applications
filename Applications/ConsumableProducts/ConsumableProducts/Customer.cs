﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsumableProducts
{
    /// <summary>
    /// A simple class representing an event visitor,
    /// providing the consumable stand application with
    /// bare minimum accessible data members.
    /// </summary>
    class Customer
    {
        /// <summary>
        /// The event visitor's unique ticket identification number.
        /// </summary>
        public int TicketId { get; private set; }

        /// <summary>
        /// The ticket (event visitor's) event account balance.
        /// </summary>
        public decimal Balance { get; private set; }

        public Customer(int ticketId, decimal balance)
        {
            TicketId = ticketId;
            Balance = balance;
        }

        public override string ToString()
        {
            return $"{TicketId}, has {Balance:C}.";
        }

        /// <summary>
        /// Decreases the visitor's event account balance by amount,
        /// inputting the result as argument for a database method
        /// that will update the balance for this specific ticket id
        /// to amount.
        /// </summary>
        /// <param name="amount"> A decimal number representing the amount to decrease balance with. </param>
        public void DecreaseBalance(decimal amount)
        {
            Balance -= amount;

            CPDatabaseMethods.UpdateBalance(Balance, TicketId);
        }
    }
}
