﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBaseMethods;
using MySql.Data.MySqlClient;
using StandNItems;

namespace ConsumableProducts
{
    class CPDatabaseMethods : DataBase
    {
        /// <summary>
        /// Inserts the stand's name into the database and returns the surrogate key (id).
        /// </summary>
        /// <param name="standName">
        /// A string representing this stand's name.
        /// </param>
        /// <returns>
        /// Returns an integer which represents a unique identification
        /// for this particular stand (surrogate key).
        /// </returns>
        public static int InsertStand(string standName)
        {
            string sql = "INSERT INTO Stand (Name)" +
                        $"VALUES ('{standName}'); SELECT last_insert_id();";

            return ExecuteScalar(sql);
        }

        /// <summary>
        /// Requires a ticket id as input and will finalize a transaction by
        /// inserting the receipt's values into the database and returning the
        /// receipt's unique identifier.
        /// </summary>
        /// <param name="ticketId">
        /// An integer representing an event visitor's (customer's) unique identification number.
        /// </param>
        /// <returns>
        /// Returns the integer surrogate key that uniquely identifies this particular receipt.
        /// </returns>
        public static int InsertReceipt(int ticketId)
        {
            string sql = "INSERT INTO receipt (idTicket)" +
                        $"VALUES ({ticketId}); SELECT last_insert_id();";

            return ExecuteScalar(sql);
        }

        /// <summary>
        /// Inserts a receipt line into the database.
        /// </summary>
        /// <param name="receiptId"> An integer respresenting the receipts identification number. </param>
        /// <param name="standId"> An integer representing the stand's identification number. </param>
        /// <param name="rl"> A receipt line object to be used to process data into the database. </param>
        public static void InsertReceiptLine(int receiptId, int standId, ReceiptLine rl)
        {
            string sql = "INSERT INTO receipt_product_line (idReceipt, idStand, idConsumableProduct, Quantity, Price)" +
            $"VALUES ({receiptId}, {standId}, {rl.ProductId}, {rl.SalesQuantity}, {rl.SalesPrice});";

            ExecuteNonQuery(sql);
        }

        /// <summary>
        /// Sets the balance to newBalance for ticekt id.
        /// </summary>
        /// <param name="newBalance"> A decimal number representing the new balance amount. </param>
        /// <param name="ticketId"> An integer representing the ticket identification number. </param>
        public static void UpdateBalance(decimal newBalance, int ticketId)
        {
            string sql = $"UPDATE ticket SET Balance = {newBalance} WHERE idTicket = {ticketId};";

            ExecuteNonQuery(sql);
        }

        /// <summary>
        /// Static method used to obtain all the consumable 
        /// products that the evenet management has entered
        /// into the database.
        /// 
        /// These consumable products from this relation provide
        /// this stand's employee with a unique consumable product
        /// identification and a name for that product.
        /// </summary>
        /// <returns>
        /// A list of consumable products with (id, name).
        /// </returns>
        public static List<Product> GetConsumableProducts()
        {
            List<Product> resultList = new List<Product>();

            string sql = "SELECT * FROM consumable_product WHERE IsRemoved = 0;";
            // Assign reader object and loop till the end to add products to the list box.
            MySqlDataReader reader = ExecuteReader(sql);
            if (reader != null)
            {
                int productId = 0;
                string productName = string.Empty;

                while (reader.Read())
                {
                    // Create product and add product to the list.
                    productId = Convert.ToInt32(reader["idConsumableProduct"]);
                    productName = reader["Name"].ToString();
                    
                    resultList.Add(new Product(productId, productName));
                }
            }
            CloseConnection();

            return resultList;
        }

        /// <summary>
        /// Static method that is used to query the database 
        /// to obtain a stand name and reconstruct a consumable
        /// products stand management class.
        /// </summary>
        /// <param name="standId">
        /// An integer representing this stand's unique identification.
        /// </param>
        /// <returns>
        /// A consumable product stand management class initialized with both
        /// the stand's unique ID and name.
        /// </returns>
        public static ConsumableStand GetConsumableProductStand(int standId)
        {
            ConsumableStand cpStand = null; 

            string sql = $"SELECT Name FROM stand WHERE idStand = {standId};";

            MySqlDataReader reader = ExecuteReader(sql);

            if (reader != null)
            {
                if (reader.Read())
                {
                    string standName = reader[0].ToString();
                    cpStand = new ConsumableStand(standId, standName);
                }
            }
            CloseConnection();

            return cpStand;
        }

        /// <summary>
        /// Obtains this particular stand's inventory by querying the database
        /// and reconstructing Consumable Product objects based on info from
        /// the database.
        /// </summary>
        /// <param name="standId">
        /// An integer representing this stand's unique identification.
        /// </param>
        /// <returns>
        ///  A list either empty or filled with Consumable products.
        /// </returns>
        public static List<ConsumableProduct> GetStandConsumableProducts(int standId)
        {
            List<ConsumableProduct> standsConsumableProducts = new List<ConsumableProduct>();

            string sql = $"SELECT * FROM stand_has_consumable_product_readable_view WHERE idStand = {standId};";

            MySqlDataReader reader = ExecuteReader(sql);
            if (reader != null)
            {
                ConsumableProduct cp;

                int productId;
                string productName;
                decimal price;
                int numberInStock;

                while (reader.Read())
                {
                    productId = Convert.ToInt32(reader[1]);
                    productName = reader[2].ToString();
                    price = Convert.ToDecimal(reader[3]);
                    numberInStock = Convert.ToInt32(reader[4]);

                    cp = new ConsumableProduct(productId, productName, price, numberInStock);

                    standsConsumableProducts.Add(cp);
                }
            }
            CloseConnection();

            return standsConsumableProducts;
        }

        /// <summary>
        /// Sets the flag IsRemoved of (standId, productId) to 1 (true)
        /// Indicating product was removed, not loading it into the list
        /// box and stand on next launch.
        /// </summary>
        /// <param name="standId"></param>
        /// <param name="productId"></param>
        public static void RemoveProduct(int standId, int productId)
        {
            string sql = $"UPDATE stand_has_consumable_product SET IsRemoved = 1 WHERE idStand = {standId} AND idConsumableProduct = {productId};";

            ExecuteNonQuery(sql);
        }

        /// <summary>
        /// First checks if said standId, productId combination is flagged as removed,
        /// if so, it updates the data of that record, otherwise it inserts a new rec-
        /// ord with the supplied information.
        /// </summary>
        /// <param name="standId"> This stand's identification numer. </param>
        /// <param name="productId"> The current product's identification number. </param>
        /// <param name="price"> The price for this product at this stand. </param>
        /// <param name="quantity"> The number of items in stock of product at stand. </param>
        public static void AddProduct(int standId, int productId, decimal price, int quantity)
        {
            int result = UpdateProduct(standId, productId, price, quantity);

            // No row was updated, insert the information we obtained.
            if (result == 0)
            {
                string sql = $"INSERT INTO stand_has_consumable_product (idStand, idConsumableProduct, Price, NumberInStock) VALUES ({standId}, {productId}, {price}, {quantity});";

                ExecuteNonQuery(sql);
            }
        }

        /// <summary>
        /// Updates the values of price and number in stock for stand, product combination.
        /// </summary>
        /// <param name="standId"> This stand's identification numer. </param>
        /// <param name="productId"> The current product's identification number. </param>
        /// <param name="price"> The price for this product at this stand. </param>
        /// <param name="quantity"> The number of items in stock of product at stand. </param>
        /// <returns></returns>
        public static int UpdateProduct(int standId, int productId, decimal price, int quantity)
        {
            // try to update if the product already exists in db.
            string sql = $"UPDATE stand_has_consumable_product SET Price = {price}, NumberInStock = {quantity}, IsRemoved = 0 WHERE idStand = {standId} AND idConsumableProduct = {productId};";

            int result = ExecuteScalar(sql);

            return result;
        }

        /// <summary>
        /// Obtains the balance for ticketid if the ticket is valid.
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns> Returns a constructed customer object. </returns>
        public static Customer GetCustomer(int ticketId)
        {
            Customer customer = null;

            string sql = $"SELECT Balance FROM ticket WHERE idTicket = {ticketId} AND IsValid = 1;";

            MySqlDataReader reader = ExecuteReader(sql);

            if (reader != null)
            {
                if (reader.Read())
                {
                    decimal balance = Convert.ToDecimal(reader[0]);
                    customer = new Customer(ticketId, balance);
                }
            }
            CloseConnection();

            return customer;
        }

        /// <summary>
        /// Obtains a customer by comparing the scanned RFID's tag to a tag in the database
        /// and obtains the corresponding ticket id.
        /// </summary>
        /// <param name="currentTag"></param>
        /// <returns> A constructed customer object. </returns>
        public static Customer ObtainTicketByTag(string currentTag, ref int consumableProductId)
        {
            // obtain ticket id.
            Customer customer = null;
            int ticketId = -1;
            bool isProduct = false;
        
            string sql = $"SELECT idTicket, IsProduct FROM rfid_to_ticket WHERE tag = '{currentTag}';";

            MySqlDataReader reader = ExecuteReader(sql);

            if (reader != null)
            {
                if (reader.Read())
                {
                    if (!(reader[0] is DBNull))
                    {
                        ticketId = Convert.ToInt32(reader[0]);
                        if (Convert.ToInt32(reader[1]) == 1)
                        {
                            isProduct = true;
                        }
                    }
                }
            }
            CloseConnection();

            if (ticketId > 0)
            {
                if (!isProduct)
                {
                    customer = GetCustomer(ticketId);
                }
                else
                {
                    consumableProductId = ticketId;
                }
            }
            return customer;
        }
    }
}
