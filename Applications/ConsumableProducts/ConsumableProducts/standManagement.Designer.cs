﻿namespace ConsumableProducts
{
    partial class standManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.productsInDb = new System.Windows.Forms.ListBox();
            this.productInsertionGbx.SuspendLayout();
            this.applicationPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // productOverview
            // 
            this.productOverview.ItemHeight = 25;
            this.productOverview.Margin = new System.Windows.Forms.Padding(8);
            this.productOverview.Size = new System.Drawing.Size(654, 479);
            this.productOverview.SelectedIndexChanged += new System.EventHandler(this.productOverview_SelectedIndexChanged);
            // 
            // addBtn
            // 
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // updateBtn
            // 
            this.updateBtn.Click += new System.EventHandler(this.updateBtn_Click);
            // 
            // removeBtn
            // 
            this.removeBtn.Click += new System.EventHandler(this.removeBtn_Click);
            // 
            // stockTb
            // 
            this.stockTb.Margin = new System.Windows.Forms.Padding(8);
            this.stockTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.stockTb_KeyPress);
            // 
            // priceTb
            // 
            this.priceTb.Margin = new System.Windows.Forms.Padding(8);
            this.priceTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.priceTb_KeyPress);
            // 
            // nameTb
            // 
            this.nameTb.Margin = new System.Windows.Forms.Padding(8);
            this.nameTb.ReadOnly = true;
            // 
            // idTb
            // 
            this.idTb.Margin = new System.Windows.Forms.Padding(8);
            this.idTb.ReadOnly = false;
            this.idTb.TextChanged += new System.EventHandler(this.idTb_TextChanged);
            this.idTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.idTb_KeyPress);
            // 
            // nameLb
            // 
            this.nameLb.Location = new System.Drawing.Point(10, 119);
            this.nameLb.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            // 
            // applicationPanel
            // 
            this.applicationPanel.Controls.Add(this.productsInDb);
            this.applicationPanel.Location = new System.Drawing.Point(24, 21);
            this.applicationPanel.Margin = new System.Windows.Forms.Padding(8);
            this.applicationPanel.Size = new System.Drawing.Size(1412, 808);
            this.applicationPanel.Controls.SetChildIndex(this.productOverview, 0);
            this.applicationPanel.Controls.SetChildIndex(this.updateBtn, 0);
            this.applicationPanel.Controls.SetChildIndex(this.addBtn, 0);
            this.applicationPanel.Controls.SetChildIndex(this.removeBtn, 0);
            this.applicationPanel.Controls.SetChildIndex(this.productInsertionGbx, 0);
            this.applicationPanel.Controls.SetChildIndex(this.productsInDb, 0);
            // 
            // productsInDb
            // 
            this.productsInDb.FormattingEnabled = true;
            this.productsInDb.ItemHeight = 25;
            this.productsInDb.Location = new System.Drawing.Point(692, 304);
            this.productsInDb.Margin = new System.Windows.Forms.Padding(4);
            this.productsInDb.Name = "productsInDb";
            this.productsInDb.Size = new System.Drawing.Size(692, 479);
            this.productsInDb.TabIndex = 6;
            this.productsInDb.SelectedIndexChanged += new System.EventHandler(this.productsInDb_SelectedIndexChanged);
            // 
            // standManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1460, 850);
            this.Margin = new System.Windows.Forms.Padding(8);
            this.Name = "standManagement";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.standManagement_FormClosing);
            this.productInsertionGbx.ResumeLayout(false);
            this.productInsertionGbx.PerformLayout();
            this.applicationPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ListBox productsInDb;
    }
}