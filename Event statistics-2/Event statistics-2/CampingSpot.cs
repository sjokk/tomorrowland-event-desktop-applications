﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event_statistics_2
{
    class CampingSpot
    {
        
        public int idCampingSpot { get; private set; }
        public String SpotName { get; set; }
        public int NumberofAvailablespot { get; set; }

        public CampingSpot(int idcampingspot, String spotName, int numberOfAvailablespot)
        {
            idCampingSpot = idcampingspot;
            SpotName = spotName;
            NumberofAvailablespot = numberOfAvailablespot;
        }

        public override string ToString()
        {
            return "Camping spot id: " + idCampingSpot + ", name: " + SpotName + ", number of available spots: " + NumberofAvailablespot;
        }
    }
}
