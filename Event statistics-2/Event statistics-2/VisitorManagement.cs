﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBaseMethods;
using MySql.Data.MySqlClient;

namespace Event_statistics_2
{
    class VisitorManagement : DataBase
    {
        public List<Visitor> visitorsPerson { get; set; }
        public List<Visitor> visitorsTrans { get; set; }
        public VisitorManagement()
        {
            visitorsPerson = new List<Visitor>();
            visitorsTrans = new List<Visitor>();
        }
        
        //visitor list for personal info
        public List<Visitor> getAllVisitorsPerson()
        {
            return visitorsPerson;
        }
        //list for transaction info
        public List<Visitor> getAllVisitorsTrans()
        {
            return visitorsTrans;
        }

        public Visitor GetVisitorbyId(int id)
        {
            foreach(Visitor v in getAllVisitorsPerson())
            {
                if (v.CustomerId == id)
                {
                    return v;
                }
            }
            return null;
        }

        public void RetrievePersonInfo()
        {
            visitorsPerson = new List<Visitor>();
            string sql =
                @"Select c.idCustomer, First_Name, Last_Name, Dob, Balance, `Total Spending per Customer`, IsValid 
                  from customer c, ticket t, spending_per_customer_view spc 
                  where c.idCustomer = t.idCustomer and c.idCustomer = spc.idCustomer"; 

            MySqlDataReader mdr = ExecuteReader(sql);

            while(mdr.Read())
            {
                int cusId = Convert.ToInt32(mdr[0]);
                String firstname = Convert.ToString(mdr[1]);
                String lastname = Convert.ToString(mdr[2]);
                DateTime dob = Convert.ToDateTime(mdr[3]);
                double balance = Convert.ToDouble(mdr[4]);
                double totalspend = Convert.ToDouble(mdr[5]);
                int isvalid = Convert.ToInt32(mdr[6]);

                Visitor v = new Visitor(cusId, firstname, lastname, dob, balance, totalspend, isvalid);
                visitorsPerson.Add(v);
            }
            CloseConnection();
        }

        public void RetrieveTransactionInfo(int id)
        {
            visitorsTrans = new List<Visitor>(); 
            string sql = 
                string.Format(@"Select ctv.idReceipt, DateTime, ctv.idTicket, Transaction_Total
                from customer_transaction_view ctv, ticket t
                where ctv.idTicket = t.idTicket and idCustomer = {0}", id);

            MySqlDataReader mdr = ExecuteReader(sql);

            while(mdr.Read())
            {
                int receiptid = Convert.ToInt32(mdr[0]);
                DateTime transdate = Convert.ToDateTime(mdr[1]);
                int ticketid = Convert.ToInt32(mdr[2]);
                int amounttrans = Convert.ToInt32(mdr[3]);

                Visitor v = new Visitor(receiptid, transdate, ticketid, amounttrans);
                visitorsTrans.Add(v);
            }
            CloseConnection();
        }

        //retrieve overall data
        public int RetrieveTotalVisitors()
        {
            string sql = "Select COUNT(idCustomer) from customer";
            return ExecuteScalar(sql);
        }

        public double RetrieveTotalBalance()
        {
            string sql = "Select SUM(Balance) from ticket";
            MySqlDataReader mdr = ExecuteReader(sql);
            double totalBalance = 0;
            while (mdr.Read())
            {
                totalBalance = Convert.ToDouble(mdr[0]);
            }
            CloseConnection();
            return totalBalance;
        }

        public double RetrieveTotalSpend()
        {
            string sql = "Select SUM(`Total Spending per Customer`) from spending_per_customer_view";
            MySqlDataReader mdr = ExecuteReader(sql);
            double totalSpend = 0;
            while (mdr.Read())
            {
                totalSpend = Convert.ToDouble(mdr[0]);
            }
            CloseConnection();
            return totalSpend;
        }
    }
}
