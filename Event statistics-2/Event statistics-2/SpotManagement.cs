﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using DataBaseMethods;

namespace Event_statistics_2
{
    class SpotManagement : DataBase
    {
        public List<CampingSpot> Campingspots { get; set; }

        public SpotManagement()
        {
            Campingspots = new List<CampingSpot>();
        }

        public List<CampingSpot> GetAllSpots()
        {
            return Campingspots;
        }

        public void RetrieveSpotData()
        {
            Campingspots = new List<CampingSpot>();
            string sql = "select cs.idCampingspot, cs.Name, acs.Name, acs.available_spots " +
                "from campingspotview cs, available_camping_spot_view_1 acs " +
                "where cs.Name = acs.Name";

            MySqlDataReader mdr = ExecuteReader(sql);

            while (mdr.Read())
            {
                int spotid = Convert.ToInt32(mdr[0]);
                String spotName = Convert.ToString(mdr[2]);
                int availablespot = Convert.ToInt32(mdr[3]); 

                CampingSpot cs = new CampingSpot(spotid, spotName, availablespot);
                Campingspots.Add(cs);
            }
            CloseConnection();
        }
    }
}
