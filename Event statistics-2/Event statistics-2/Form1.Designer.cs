﻿namespace Event_statistics_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.SpotsViewlbx = new System.Windows.Forms.ListBox();
            this.LoadAllSpotsbtn = new System.Windows.Forms.Button();
            this.LoadStandsbtn = new System.Windows.Forms.Button();
            this.StandRevenueViewChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TotalVisitorslb = new System.Windows.Forms.Label();
            this.TotalBalancelb = new System.Windows.Forms.Label();
            this.TotalSpendMoneylb = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.CustomerinfoViewbtn = new System.Windows.Forms.Button();
            this.CustomerIdtbx = new System.Windows.Forms.TextBox();
            this.PersonalInfolbx = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ProductViewChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.ProductViewbtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.StandRevenueViewChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProductViewChart)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(462, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "Camping spots view";
            // 
            // SpotsViewlbx
            // 
            this.SpotsViewlbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpotsViewlbx.FormattingEnabled = true;
            this.SpotsViewlbx.HorizontalScrollbar = true;
            this.SpotsViewlbx.ItemHeight = 22;
            this.SpotsViewlbx.Location = new System.Drawing.Point(465, 57);
            this.SpotsViewlbx.Name = "SpotsViewlbx";
            this.SpotsViewlbx.Size = new System.Drawing.Size(285, 312);
            this.SpotsViewlbx.TabIndex = 2;
            // 
            // LoadAllSpotsbtn
            // 
            this.LoadAllSpotsbtn.Location = new System.Drawing.Point(466, 387);
            this.LoadAllSpotsbtn.Name = "LoadAllSpotsbtn";
            this.LoadAllSpotsbtn.Size = new System.Drawing.Size(284, 47);
            this.LoadAllSpotsbtn.TabIndex = 3;
            this.LoadAllSpotsbtn.Text = "Load all spots";
            this.LoadAllSpotsbtn.UseVisualStyleBackColor = true;
            this.LoadAllSpotsbtn.Click += new System.EventHandler(this.LoadAllSpotsbtn_Click);
            // 
            // LoadStandsbtn
            // 
            this.LoadStandsbtn.Location = new System.Drawing.Point(1078, 387);
            this.LoadStandsbtn.Name = "LoadStandsbtn";
            this.LoadStandsbtn.Size = new System.Drawing.Size(181, 47);
            this.LoadStandsbtn.TabIndex = 4;
            this.LoadStandsbtn.Text = "Stands view update";
            this.LoadStandsbtn.UseVisualStyleBackColor = true;
            this.LoadStandsbtn.Click += new System.EventHandler(this.LoadStandsbtn_Click);
            // 
            // StandRevenueViewChart
            // 
            chartArea1.Name = "ChartArea1";
            this.StandRevenueViewChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.StandRevenueViewChart.Legends.Add(legend1);
            this.StandRevenueViewChart.Location = new System.Drawing.Point(792, 57);
            this.StandRevenueViewChart.Name = "StandRevenueViewChart";
            series1.ChartArea = "ChartArea1";
            series1.Color = System.Drawing.Color.Green;
            series1.Legend = "Legend1";
            series1.Name = "Stand Revenue";
            this.StandRevenueViewChart.Series.Add(series1);
            this.StandRevenueViewChart.Size = new System.Drawing.Size(467, 300);
            this.StandRevenueViewChart.TabIndex = 5;
            this.StandRevenueViewChart.Text = "chart1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(788, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 22);
            this.label2.TabIndex = 6;
            this.label2.Text = "Stand revenue view";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Total visitors:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Total balance: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(151, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "Total spend money: ";
            // 
            // TotalVisitorslb
            // 
            this.TotalVisitorslb.AutoSize = true;
            this.TotalVisitorslb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalVisitorslb.Location = new System.Drawing.Point(118, 22);
            this.TotalVisitorslb.Name = "TotalVisitorslb";
            this.TotalVisitorslb.Size = new System.Drawing.Size(20, 22);
            this.TotalVisitorslb.TabIndex = 10;
            this.TotalVisitorslb.Text = "a";
            // 
            // TotalBalancelb
            // 
            this.TotalBalancelb.AutoSize = true;
            this.TotalBalancelb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalBalancelb.Location = new System.Drawing.Point(130, 65);
            this.TotalBalancelb.Name = "TotalBalancelb";
            this.TotalBalancelb.Size = new System.Drawing.Size(20, 22);
            this.TotalBalancelb.TabIndex = 11;
            this.TotalBalancelb.Text = "b";
            // 
            // TotalSpendMoneylb
            // 
            this.TotalSpendMoneylb.AutoSize = true;
            this.TotalSpendMoneylb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalSpendMoneylb.Location = new System.Drawing.Point(169, 108);
            this.TotalSpendMoneylb.Name = "TotalSpendMoneylb";
            this.TotalSpendMoneylb.Size = new System.Drawing.Size(19, 22);
            this.TotalSpendMoneylb.TabIndex = 12;
            this.TotalSpendMoneylb.Text = "c";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(307, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Please enter personal id to view your info:  ";
            // 
            // CustomerinfoViewbtn
            // 
            this.CustomerinfoViewbtn.Location = new System.Drawing.Point(325, 187);
            this.CustomerinfoViewbtn.Name = "CustomerinfoViewbtn";
            this.CustomerinfoViewbtn.Size = new System.Drawing.Size(100, 37);
            this.CustomerinfoViewbtn.TabIndex = 14;
            this.CustomerinfoViewbtn.Text = "View";
            this.CustomerinfoViewbtn.UseVisualStyleBackColor = true;
            this.CustomerinfoViewbtn.Click += new System.EventHandler(this.CustomerinfoViewbtn_Click);
            // 
            // CustomerIdtbx
            // 
            this.CustomerIdtbx.Location = new System.Drawing.Point(325, 146);
            this.CustomerIdtbx.Name = "CustomerIdtbx";
            this.CustomerIdtbx.Size = new System.Drawing.Size(100, 26);
            this.CustomerIdtbx.TabIndex = 15;
            // 
            // PersonalInfolbx
            // 
            this.PersonalInfolbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PersonalInfolbx.FormattingEnabled = true;
            this.PersonalInfolbx.HorizontalScrollbar = true;
            this.PersonalInfolbx.ItemHeight = 22;
            this.PersonalInfolbx.Location = new System.Drawing.Point(16, 246);
            this.PersonalInfolbx.Name = "PersonalInfolbx";
            this.PersonalInfolbx.Size = new System.Drawing.Size(409, 510);
            this.PersonalInfolbx.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(661, 460);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 22);
            this.label7.TabIndex = 17;
            this.label7.Text = "Product view";
            // 
            // ProductViewChart
            // 
            chartArea2.Name = "ChartArea1";
            this.ProductViewChart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.ProductViewChart.Legends.Add(legend2);
            this.ProductViewChart.Location = new System.Drawing.Point(792, 460);
            this.ProductViewChart.Name = "ProductViewChart";
            this.ProductViewChart.RightToLeft = System.Windows.Forms.RightToLeft.No;
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Product Revenue";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "totalSold";
            this.ProductViewChart.Series.Add(series2);
            this.ProductViewChart.Series.Add(series3);
            this.ProductViewChart.Size = new System.Drawing.Size(467, 288);
            this.ProductViewChart.TabIndex = 18;
            this.ProductViewChart.Text = "chart1";
            // 
            // ProductViewbtn
            // 
            this.ProductViewbtn.Location = new System.Drawing.Point(602, 495);
            this.ProductViewbtn.Name = "ProductViewbtn";
            this.ProductViewbtn.Size = new System.Drawing.Size(172, 47);
            this.ProductViewbtn.TabIndex = 19;
            this.ProductViewbtn.Text = "Product view update";
            this.ProductViewbtn.UseVisualStyleBackColor = true;
            this.ProductViewbtn.Click += new System.EventHandler(this.ProductViewbtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1282, 759);
            this.Controls.Add(this.ProductViewbtn);
            this.Controls.Add(this.ProductViewChart);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.PersonalInfolbx);
            this.Controls.Add(this.CustomerIdtbx);
            this.Controls.Add(this.CustomerinfoViewbtn);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TotalSpendMoneylb);
            this.Controls.Add(this.TotalBalancelb);
            this.Controls.Add(this.TotalVisitorslb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.StandRevenueViewChart);
            this.Controls.Add(this.LoadStandsbtn);
            this.Controls.Add(this.LoadAllSpotsbtn);
            this.Controls.Add(this.SpotsViewlbx);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Event statistics";
            ((System.ComponentModel.ISupportInitialize)(this.StandRevenueViewChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProductViewChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox SpotsViewlbx;
        private System.Windows.Forms.Button LoadAllSpotsbtn;
        private System.Windows.Forms.Button LoadStandsbtn;
        private System.Windows.Forms.DataVisualization.Charting.Chart StandRevenueViewChart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label TotalVisitorslb;
        private System.Windows.Forms.Label TotalBalancelb;
        private System.Windows.Forms.Label TotalSpendMoneylb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button CustomerinfoViewbtn;
        private System.Windows.Forms.TextBox CustomerIdtbx;
        private System.Windows.Forms.ListBox PersonalInfolbx;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataVisualization.Charting.Chart ProductViewChart;
        private System.Windows.Forms.Button ProductViewbtn;
    }
}

