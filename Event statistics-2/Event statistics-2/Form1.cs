﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Event_statistics_2
{
    public partial class Form1 : Form
    {
        private SpotManagement sm;
        private VisitorManagement vm;

        private static string connectionInfo = "Server=studmysql01.fhict.local;" +
                                               "Uid=dbi231896;" +
                                               "Database=dbi231896;" +
                                               "Pwd=UnsafePassword;";

        MySqlConnection connection = new MySqlConnection(connectionInfo);

        public Form1()
        {
            InitializeComponent();
            sm = new SpotManagement();
            vm = new VisitorManagement();
            ShowNumberVisitosInLb();
            ShowTotalBalanceInLb();
            ShowTotalSpendInLb();
            LoadStandChart();
            ProductChart();
        }

        //show camping spots view
        private void LoadAllSpotsbtn_Click(object sender, EventArgs e)
        {
            SpotsViewlbx.Items.Clear();
            sm.RetrieveSpotData();
            foreach(CampingSpot cs in sm.GetAllSpots())
            {
                SpotsViewlbx.Items.Add(cs);
            }
        }

        //show stand view in chart with columns
        private void LoadStandsbtn_Click(object sender, EventArgs e)
        {
            StandRevenueViewChart.Series["Stand Revenue"].Points.Clear();
            LoadStandChart();
        }
        public void LoadStandChart()
        {
            connection.Open();
            try
            {
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Select * from stand_revenue_view";
                MySqlDataReader mdr = cmd.ExecuteReader();

                while(mdr.Read())
                {
                    StandRevenueViewChart.Series["Stand Revenue"].Points.AddXY(mdr.GetString("Name"), mdr.GetDecimal("Stand Revenue"));
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Error loading chart!");
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        //show visitors view
        private void CustomerinfoViewbtn_Click(object sender, EventArgs e)
        {           
            try
            {
                PersonalInfolbx.Items.Clear();
                int cusId = Convert.ToInt32(CustomerIdtbx.Text);
                vm.RetrievePersonInfo();
                vm.RetrieveTransactionInfo(cusId);
                Visitor v = vm.GetVisitorbyId(cusId);

                if (v != null)
                {
                    string rt = v.ToString();
                    /*
                     * use split because there is no easy way to split a whole string 
                     * into several lines in the listbox
                    */
                    string[] splitToLines = rt.Split(new[] { '\n' }); 
                    foreach(string split in splitToLines)
                    {
                        if (split.Trim() == "")
                            continue;
                        PersonalInfolbx.Items.Add(split.Trim());
                    }                    
                    PersonalInfolbx.Items.Add("------------------------------------");
                    foreach (Visitor vs in vm.getAllVisitorsTrans())
                    {
                        PersonalInfolbx.Items.Add(vs.TransactionDetails());
                    }
                }
                else
                {
                    MessageBox.Show("No matching visitor!");
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Please enter your id to view info!");
            }
        }

        //show products view in chart with columns
        private void ProductViewbtn_Click(object sender, EventArgs e)
        {
            ProductViewChart.Series["Product Revenue"].Points.Clear();
            ProductViewChart.Series["totalSold"].Points.Clear();
            ProductChart();
        }
        public void ProductChart()
        {
            connection.Open();
            try
            {
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Select * from product_revenue_view";
                MySqlDataReader mdr = cmd.ExecuteReader();

                while (mdr.Read())
                {
                    ProductViewChart.Series["Product Revenue"].Points.AddXY(mdr.GetString("Name"), mdr.GetDecimal("Product Revenue"));
                    ProductViewChart.Series["totalSold"].Points.AddXY(mdr.GetString("Name"), mdr.GetDecimal("totalSold"));
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error loading chart!");
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        //show overall view in labels
        private void ShowNumberVisitosInLb()
        {
            TotalVisitorslb.Text = vm.RetrieveTotalVisitors().ToString();
        }

        private void ShowTotalBalanceInLb()
        {
            TotalBalancelb.Text = vm.RetrieveTotalBalance().ToString();
        }

        private void ShowTotalSpendInLb()
        {
            TotalSpendMoneylb.Text = vm.RetrieveTotalSpend().ToString();
        }
    }
}
