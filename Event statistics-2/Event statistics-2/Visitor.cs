﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event_statistics_2
{
    class Visitor
    {
        //overall visitors info
        public int TotalVisitors { get; set; }
        public double TotalBalance { get; set; }
        public double TotalSpendMoney { get; set; }

        //personal info
        public int CustomerId { get; private set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public DateTime DOB { get; set; }
        public double Balance { get; set; }
        //show if the ticket is still valid or not
        public int IsValid { get; set; }
        public int ReceiptId { get; private set; }
        public DateTime TransactionTime { get; set; }
        public int TicketId { get; set; }
        public double TransactionDetail { get; set; }
        public double TotalSpendingPerCustomer { get; set; }

        //personal info
        public Visitor(int customerid, String firstName, String lastName, DateTime dob, double balance, double totalspendperCustomer, int isValid)
        {
            CustomerId = customerid;
            FirstName = firstName;
            LastName = lastName;
            DOB = dob;
            Balance = balance;
            TotalSpendingPerCustomer = totalspendperCustomer;
            IsValid = isValid;
        }

        //transaction detail
        public Visitor(int receiptId, DateTime transactionTime, int ticketid, double transactionDetail)
        {
            ReceiptId = receiptId;
            TransactionTime = transactionTime;
            TicketId = ticketid;
            TransactionDetail = transactionDetail;
        }

        public override string ToString()
        {
            if (IsValid == 1)
            {
                return "Your name: " + FirstName + " " + LastName +
                "\nYour date of birth: " + DOB +
                "\nYour current balance: " + Balance +
                "\nThe total money you spent so far during the event: " + TotalSpendingPerCustomer;
            }
            else
            {
                return "Your name: " + FirstName + " " + LastName +
                "\nYour date of birth: " + DOB +
                "\nYour current balance: " + Balance +
                "\nThe total money you spent so far during the event: " + TotalSpendingPerCustomer +
                "\nYou have invalid ticket.";
            }
        }

        public string TransactionDetails()
        {
            return "Receipt id: " + ReceiptId + ", transaction time: " + TransactionTime +
                ", ticket id: " + TicketId + ", amount: " + TransactionDetail;
        }
    }
}
