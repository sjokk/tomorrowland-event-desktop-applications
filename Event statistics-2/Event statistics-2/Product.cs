﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event_statistics_2
{
    class Product
    {
        public int ProductId { get; private set; }
        public String ProductName { get; set; }
        public double Revenue { get; set; }

        //public int StandId { get; private set; }
        public int AmountEachSold { get; set; }

        public Product(int productId, String productName, double revenue, int amountEachSold)
        {
            ProductId = productId;
            ProductName = productName;
            Revenue = revenue;
            AmountEachSold = amountEachSold;
        }
    }
}
