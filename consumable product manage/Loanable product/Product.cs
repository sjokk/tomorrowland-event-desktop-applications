﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loanable_product
{
    class Product
    {
        //property
        public int ID { get; private set; }
        public String Name { get; set; }
        
        //constructor
        public Product(int id, String name)
        {
            ID = id;
            Name = name;
        }

        //method
        public override string ToString()
        {
            return "ID: " + ID + ", Name: " + Name;
        }
    }
}
