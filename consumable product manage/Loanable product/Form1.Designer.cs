﻿namespace Loanable_product
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label2 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LoadAllbtn = new System.Windows.Forms.Button();
            this.SearchNametbx = new System.Windows.Forms.TextBox();
            this.SearchIdtbx = new System.Windows.Forms.TextBox();
            this.SearchNamebtn = new System.Windows.Forms.Button();
            this.SearchIDbtn = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.lbxProduct = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(77, 98);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(134, 26);
            this.tbName.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.LoadAllbtn);
            this.groupBox1.Controls.Add(this.SearchNametbx);
            this.groupBox1.Controls.Add(this.SearchIdtbx);
            this.groupBox1.Controls.Add(this.SearchNamebtn);
            this.groupBox1.Controls.Add(this.SearchIDbtn);
            this.groupBox1.Controls.Add(this.btnAdd);
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Controls.Add(this.btnRemove);
            this.groupBox1.Controls.Add(this.lbxProduct);
            this.groupBox1.Controls.Add(this.tbName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(13, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(617, 642);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Consumable product table";
            // 
            // LoadAllbtn
            // 
            this.LoadAllbtn.Location = new System.Drawing.Point(322, 237);
            this.LoadAllbtn.Name = "LoadAllbtn";
            this.LoadAllbtn.Size = new System.Drawing.Size(149, 47);
            this.LoadAllbtn.TabIndex = 13;
            this.LoadAllbtn.Text = "Load All";
            this.LoadAllbtn.UseVisualStyleBackColor = true;
            this.LoadAllbtn.Click += new System.EventHandler(this.LoadAllbtn_Click);
            // 
            // SearchNametbx
            // 
            this.SearchNametbx.Location = new System.Drawing.Point(490, 417);
            this.SearchNametbx.Name = "SearchNametbx";
            this.SearchNametbx.Size = new System.Drawing.Size(115, 26);
            this.SearchNametbx.TabIndex = 12;
            // 
            // SearchIdtbx
            // 
            this.SearchIdtbx.Location = new System.Drawing.Point(490, 338);
            this.SearchIdtbx.Name = "SearchIdtbx";
            this.SearchIdtbx.Size = new System.Drawing.Size(115, 26);
            this.SearchIdtbx.TabIndex = 11;
            // 
            // SearchNamebtn
            // 
            this.SearchNamebtn.Location = new System.Drawing.Point(322, 417);
            this.SearchNamebtn.Name = "SearchNamebtn";
            this.SearchNamebtn.Size = new System.Drawing.Size(149, 47);
            this.SearchNamebtn.TabIndex = 10;
            this.SearchNamebtn.Text = "Search by Name";
            this.SearchNamebtn.UseVisualStyleBackColor = true;
            this.SearchNamebtn.Click += new System.EventHandler(this.SearchNamebtn_Click);
            // 
            // SearchIDbtn
            // 
            this.SearchIDbtn.Location = new System.Drawing.Point(322, 338);
            this.SearchIDbtn.Name = "SearchIDbtn";
            this.SearchIDbtn.Size = new System.Drawing.Size(149, 47);
            this.SearchIDbtn.TabIndex = 9;
            this.SearchIDbtn.Text = "Search by ID";
            this.SearchIDbtn.UseVisualStyleBackColor = true;
            this.SearchIDbtn.Click += new System.EventHandler(this.SearchIDbtn_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(11, 164);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(200, 47);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(322, 494);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(149, 47);
            this.btnUpdate.TabIndex = 7;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click_1);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(322, 572);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(149, 49);
            this.btnRemove.TabIndex = 8;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // lbxProduct
            // 
            this.lbxProduct.FormattingEnabled = true;
            this.lbxProduct.ItemHeight = 20;
            this.lbxProduct.Location = new System.Drawing.Point(10, 237);
            this.lbxProduct.Name = "lbxProduct";
            this.lbxProduct.Size = new System.Drawing.Size(281, 384);
            this.lbxProduct.TabIndex = 5;
            this.lbxProduct.SelectedIndexChanged += new System.EventHandler(this.lbxProduct_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(641, 663);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.142858F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Consumable product management";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.ListBox lbxProduct;
        private System.Windows.Forms.Button SearchNamebtn;
        private System.Windows.Forms.Button SearchIDbtn;
        private System.Windows.Forms.TextBox SearchNametbx;
        private System.Windows.Forms.TextBox SearchIdtbx;
        private System.Windows.Forms.Button LoadAllbtn;
    }
}

