﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBaseMethods;
using MySql.Data.MySqlClient;
using StandNItems;

namespace Loanable_product
{
    class ProductManagement : DataBase
    {
        //property
        public List<Product> products { get; set; }

        //constructor
        public ProductManagement()
        {
            products = new List<Product>();
        }

        //method
        /*if there exists a product with name equal to name,
         * return that specific product, else, null is returned
         */
        public Product GetProduct(String name)
        {
            foreach (Product p in products)
            {
                if (p.Name == name)
                {
                    return p;
                }
            }
            return null;
        }

        //retrieve data from database and add them to the list
        public void RetrieveAllDataFromDB()
        {
            string sql = "select * from consumable_product where IsRemoved = 0;";

            MySqlDataReader mdr = ExecuteReader(sql);

            while (mdr.Read())
            {
                int id = Convert.ToInt32(mdr[0]);
                string name = Convert.ToString(mdr[1]);

                Product p = new Product(id, name);
                products.Add(p);
            }
            CloseConnection();
        }
        /*
         * if there is no product with name equal to p.Name, 
         * then p is added to the list and return value is true
         * else, nothing is added and return value is false
        */
        public int AddProduct(string name)
        {
            string sql = $"INSERT INTO consumable_product(Name) values ('{name}'); select last_insert_id();";
            return ExecuteScalar(sql);
        }
        /*
         * select a row of info in the lb and its id and name will be shown
         * in the textboxes. The name can be changed 
         * after clicking the 'update' btn
        */
        public void UpdateProduct(Product p)
        {
            if (GetAllProducts() != null)
            {
                string sql = $"UPDATE consumable_product SET Name = '{p.Name}', IsRemoved = 0 WHERE idConsumableProduct = {p.ID};";
                ExecuteNonQuery(sql);
            }
        }
        /* 
         * remove a certain product from the database as selected in the lbx
        */
        public void RemoveProduct(Product p)
        {
            if (GetAllProducts() != null)
            {
                GetAllProducts().Remove(p); //first remove the product from the list

                string sql = $"UPDATE consumable_product SET IsRemoved = 1 WHERE idConsumableProduct = {p.ID};";
                ExecuteNonQuery(sql);
            }
        }

        public List<Product> GetAllProducts()
        {
            return products;
        }

        //Sorting & Search
        /*
         * search by product id
         * since the list is sorted by id automatically 
         * so there is no neccessary to sort by id
        */
        public void SortById()
        {//in case the list is sorted by name after search by name
            Product temp;
            for(int i = 1; i < products.Count; i++)
            {
                for(int j = 0; j < products.Count - i; j++)
                {
                    if (products[j].ID > products[j + 1].ID)
                    {
                        temp = products[j];
                        products[j] = products[j + 1];
                        products[j + 1] = temp;
                    }
                }
            }
        }
        private Product BinarySearchId(int id, int lowindex, int highindex)
        {
            int m;
            while (lowindex <= highindex)
            {
                m = (lowindex + highindex) / 2;
                if (products[m].ID == id)
                {
                    return products[m];
                }
                else if (products[m].ID < id)
                {
                    return BinarySearchId(id, m + 1, highindex);
                }
                else
                {
                    return BinarySearchId(id, lowindex, m - 1);
                }
            }
            return null;
        }
        public Product FindProductById(int id)
        {
            return BinarySearchId(id, 0, products.Count - 1);
        }

        //sort the elements of the product list in alphabetically
        public void SortByName()
        {
            Product temp;
            for (int i = 1; i <= products.Count; i++)
            {
                for (int j = 0; j < products.Count - i; j++)
                {
                    if (string.Compare(products[j + 1].Name, products[j].Name) == -1)
                    {
                        temp = products[j];
                        products[j] = products[j + 1];
                        products[j + 1] = temp;
                    }
                }
            }
        }
        private Product BinarySearchByName(string name, int lowindex, int highindex)
        {
            int m;
            while (lowindex <= highindex)
            {
                m = (lowindex + highindex) / 2;
                if (products[m].Name == name)
                {
                    return products[m];
                }
                else if (string.Compare(products[m].Name, name) == -1)
                {
                    return BinarySearchByName(name, m + 1, highindex);
                }
                else
                {
                    return BinarySearchByName(name, lowindex, m - 1);
                }
            }
            return null;
        }
        public Product FindProductByName(string name)
        {
            SortByName();
            return BinarySearchByName(name, 0, products.Count - 1);
        }
    }
}
