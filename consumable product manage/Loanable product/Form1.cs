﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using DataBaseMethods;

namespace Loanable_product
{
    public partial class Form1 : Form
    {
        private ProductManagement pm;
        public Form1()
        {
            InitializeComponent();

            pm = new ProductManagement();
            //first retrieve data from database and show them in the lbx when the app is loaded
            pm.RetrieveAllDataFromDB();
        }

        /*add a specific product to the database
         * and show its info in the listbox
        */
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                String inputName = tbName.Text;
                
                Product p = pm.GetProduct(inputName);

                if (p == null)
                {
                    p = new Product(pm.AddProduct(inputName), inputName);
                    lbxProduct.Items.Add(p); //show in the lbx
                    pm.GetAllProducts().Add(p); //add the product in the list
                    LoadAll();
                    MessageBox.Show("Product successfully added!");                   
                }
                else
                {
                    MessageBox.Show("There exists a product in the list!");
                }
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Please enter the name of the product you want to add!");
            }
            catch (myException me)
            {
                MessageBox.Show(me.Message);
            }
        }
        /*
         * remove a certain product selected in the lbx and from database
        */
        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                string inputName = tbName.Text;
                Product selectedP = (Product)lbxProduct.SelectedItem;

                if (selectedP != null)
                {
                    lbxProduct.Items.Remove(selectedP);
                    pm.RemoveProduct(selectedP);
                    UpdateProductsOverview();
                }
                else
                {
                    MessageBox.Show("Please select the name of the product you want to remove!");
                }
            }
            catch (myException me)
            {
                MessageBox.Show(me.Message);
            }
        }
        /*
         * select a row of info in the lb and its id and name will be shown 
         * in the textboxes. The name can be changed 
         * after clicking the 'update' btn
         */
        private void btnUpdate_Click_1(object sender, EventArgs e)
        {
            try
            {
                string inputName = tbName.Text;
                Product selectedP = (Product)lbxProduct.SelectedItem;
                selectedP.Name = inputName;

                if (selectedP != null)
                {
                    pm.UpdateProduct(selectedP);
                    UpdateProductsOverview();
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Please select the name of the product you want to update");
            }
            catch (myException me)
            {
                MessageBox.Show(me.Message);
            }
        }
        /*
         * method used to update the lbx after updating the list and database
        */
        private void UpdateProductsOverview()
        {
            lbxProduct.Items.Clear();

            foreach (Product p in pm.GetAllProducts())
            {
                lbxProduct.Items.Add(p);
            }
        }
        /*
         * show the info of the specific product in the tbx when selecting in lbx
        */
        private void lbxProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxProduct.SelectedIndex != -1)
            {
                if (lbxProduct.SelectedItem is Product)
                {
                    tbName.Text = ((Product)lbxProduct.SelectedItem).Name;
                }
                else
                {
                    MessageBox.Show("a");
                }
            }
        }

        //Sort & Search
        private void SearchIDbtn_Click(object sender, EventArgs e)
        {
            try
            {                
                if (pm.FindProductById(Convert.ToInt32(SearchIdtbx.Text)) != null)
                {
                    lbxProduct.Items.Clear();
                    lbxProduct.Items.Add(pm.FindProductById(Convert.ToInt32(SearchIdtbx.Text)).ToString());
                }
                else
                {
                    MessageBox.Show("Product with such id doesn't exist!");
                }
            }
            catch(FormatException)
            {
                MessageBox.Show("Please enter the id of the product you want to search!");
            }           
        }

        private void SearchNamebtn_Click(object sender, EventArgs e)
        {
            lbxProduct.Items.Clear();
            if (SearchNametbx.Text != null)
            {
                if (pm.FindProductByName(SearchNametbx.Text) != null)
                {
                    lbxProduct.Items.Add(pm.FindProductByName(SearchNametbx.Text).ToString());
                }
                else
                {
                    MessageBox.Show("Product with such name doesn't exist!");
                }
            }
            else
            {
                MessageBox.Show("Please enter valid name!");
            }
        }

        //load all the information of the product after searching for id or name
        private void LoadAllbtn_Click(object sender, EventArgs e)
        {
            lbxProduct.Items.Clear();
            pm.SortById();

            foreach (Product p in pm.GetAllProducts())
            {
                lbxProduct.Items.Add(p);
            }
        }

        private void LoadAll()
        {
            lbxProduct.Items.Clear();

            foreach (Product p in pm.GetAllProducts())
            {
                lbxProduct.Items.Add(p);
            }
        }
    }
}
