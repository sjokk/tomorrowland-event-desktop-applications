﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace generalGUI
{
    class CheckInOut
    {
        public int idTicket
        { get; set; }

        public int isCheckedIn
        { get; set; }

        public string FirstName
        { get; set; }

        public string LastName
        { get; set; }

        public string DateTime
        { get; set; }

        public CheckInOut(int ticketid, int ischeckedin, string firstname, string lastname, string datetime)
        {
            this.idTicket = ticketid;
            this.isCheckedIn = ischeckedin;
            this.FirstName = firstname;
            this.LastName = lastname;
            this.DateTime = datetime;
        }

       

        public override string ToString()
        {
            return ($"{DateTime}, {this.FirstName} {this.LastName}");
        }


    }
}
