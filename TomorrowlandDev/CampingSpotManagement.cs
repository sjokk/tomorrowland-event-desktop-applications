﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using MySql.Data;
using DataBaseMethods;

namespace generalGUI
{
    class CampingSpotManagement : DataBase
    {
        private List<CampingSpot> allCampingSpots = new List<CampingSpot>();
        private List<CheckInOut> CheckedIn_OutData = new List<CheckInOut>();

        public CampingSpot GetCampingSpot(int id)
        {
            foreach (CampingSpot c in allCampingSpots)
            {
                if(c.ID == id)
                {
                    return c;
                }
            }
            return null;
        }


        public CheckInOut findPerson(string name)
        {
            return CheckedIn_OutData.Find(c => $"{c.FirstName} {c.LastName}".ToLower() == name.ToLower());
        }
        public void RetrieveCheckInOutData()
        {
            string sql = "select t.idTicket,p.isCheckedIn,c.first_name,c.last_name, MAX(p.DateTime) from customer c join ticket t on (c.idCustomer = t.idCustomer) join camping_check_in_out p on (t.idTicket = p.idTicket) GROUP by c.First_Name,c.Last_Name";
            MySqlDataReader mdr = ExecuteReader(sql);

            while (mdr.Read())
            {
                int ticketid = Convert.ToInt32(mdr[0]);
                string fname = Convert.ToString(mdr[2]);
                int isCheckedin = Convert.ToInt32(mdr[1]);
                string lname = Convert.ToString(mdr[3]);
                string datetime = Convert.ToString(mdr[4]);

                string info4 = ($"{isCheckedin} {datetime}, {fname} {lname}");
                CheckInOut cio = new CheckInOut(ticketid, isCheckedin, fname, lname, datetime);
                CheckedIn_OutData.Add(cio);
            }
            CloseConnection();

            

        }
        


        public void RetrieveAllCampingSpotsfromdb()
        {
            string sql = "select * from campingspot; ";

            MySqlDataReader mdr = ExecuteReader(sql);
                while(mdr.Read())
            {
                int campingspotid = Convert.ToInt32(mdr[0]);
                string campingspotname = Convert.ToString(mdr[1]);

                allCampingSpots.Add(new CampingSpot(campingspotid, campingspotname));
            }

          
            foreach (CampingSpot item in allCampingSpots)
            {
                item.RetrieveCSpotData(item.Campingspotname);
            }

            CloseConnection();

        
        }


        public List<CampingSpot> ReturnCampingSpots()
        {
            return this.allCampingSpots;
        }

        public bool HandleParticipantsOnAdd(int ticketId, int CampingSpotId)
        {
            //first check if this participant is in any of the camping spots,if he is, remove him
            CampingSpot c = GetCampingSpotByParticipant(ticketId);
            //Get the camping spot that he wants to go in to..
            CampingSpot cc = GetCampingSpot(CampingSpotId);
            if(c == null)
            {
                
                cc.AddParticipant(ticketId);
                string sql = ($"update ticket set idCampingspot = {CampingSpotId} where idTicket = {ticketId};");
                ExecuteNonQuery(sql);
                return true;
            }
            //in this case, this participant is already in a camping spot, so we need to remove him from that camping spot and add him to the desired one
            else if(c != null)
            {
                Participant p = c.GetParticipant(ticketId);
                c.GetAllParticipants().Remove(p);
                cc.AddParticipant(ticketId);
                string sql = ($"update ticket set idCampingspot = {CampingSpotId} where idTicket = {ticketId};");
                ExecuteNonQuery(sql);
                return true;
            }


            return false;
                    //in this case, we also check if he is not in a camping spot yet
                   

        }

        public CampingSpot GetCampingSpotByParticipant(int pid)
        {
            foreach (CampingSpot c in this.allCampingSpots)
            {
                foreach (Participant p in c.GetAllParticipants())
                {
                    if(p.IDTicket == pid)
                    {
                        return c;
                    }
                }
            }
            return null;
        }

        public string RetrieveTicketIDFromRFID(string RFID)
        {
            string sql = ($"select * from rfid_to_ticket where tag = '{RFID}'");
            MySqlDataReader mdr = ExecuteReader(sql);
            string idticket = null;

            while (mdr.Read())
            {
                idticket = Convert.ToString(mdr[1]);
            }

            CloseConnection();
            return idticket;
        }
    }
}
