﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace generalGUI
{
    class Participant
    {
       public int IDTicket
        { get; set; }
        public string FirstName
        { get; set; }

        public string LastName
        { get; set; }

        public Participant(int id,string firstname, string lastname)
        {
            this.FirstName = firstname;
            this.LastName = lastname;
            this.IDTicket = id;
        }

        


        public override string ToString()
        {
            return ($" {this.FirstName} {this.LastName}");
        }
    }
}
