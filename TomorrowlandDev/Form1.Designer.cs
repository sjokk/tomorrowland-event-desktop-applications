﻿namespace generalGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.rdbCollapseCheckInOut = new System.Windows.Forms.RadioButton();
            this.rdbCreateReservation = new System.Windows.Forms.RadioButton();
            this.lblTicketIDCO = new System.Windows.Forms.Label();
            this.btnCheckIn = new System.Windows.Forms.Button();
            this.tbTicketID = new System.Windows.Forms.TextBox();
            this.panelCheckInOut = new System.Windows.Forms.Panel();
            this.btnCheckOut = new System.Windows.Forms.Button();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.panelReserve = new System.Windows.Forms.Panel();
            this.btnAdd = new System.Windows.Forms.Button();
            this.tbCampingSpotID = new System.Windows.Forms.TextBox();
            this.lblCampingspotnr = new System.Windows.Forms.Label();
            this.lblCamp15 = new System.Windows.Forms.Label();
            this.lblCamp14 = new System.Windows.Forms.Label();
            this.lblCamp13 = new System.Windows.Forms.Label();
            this.lblCamp10 = new System.Windows.Forms.Label();
            this.lbCamp12 = new System.Windows.Forms.ListBox();
            this.lblCamp12 = new System.Windows.Forms.Label();
            this.lbCamp11 = new System.Windows.Forms.ListBox();
            this.lblCamp11 = new System.Windows.Forms.Label();
            this.lbCamp10 = new System.Windows.Forms.ListBox();
            this.lbCamp15 = new System.Windows.Forms.ListBox();
            this.lbCamp14 = new System.Windows.Forms.ListBox();
            this.lbCamp13 = new System.Windows.Forms.ListBox();
            this.lbCamp9 = new System.Windows.Forms.ListBox();
            this.lblCamp9 = new System.Windows.Forms.Label();
            this.lbCamp8 = new System.Windows.Forms.ListBox();
            this.lblCamp8 = new System.Windows.Forms.Label();
            this.lbCamp7 = new System.Windows.Forms.ListBox();
            this.lblCamp7 = new System.Windows.Forms.Label();
            this.lbCamp6 = new System.Windows.Forms.ListBox();
            this.lblCamp6 = new System.Windows.Forms.Label();
            this.lbCamp5 = new System.Windows.Forms.ListBox();
            this.lblCamp5 = new System.Windows.Forms.Label();
            this.lbCamp4 = new System.Windows.Forms.ListBox();
            this.lblCamp4 = new System.Windows.Forms.Label();
            this.lbCamp3 = new System.Windows.Forms.ListBox();
            this.lblCamp3 = new System.Windows.Forms.Label();
            this.lbCamp2 = new System.Windows.Forms.ListBox();
            this.lbCamp1 = new System.Windows.Forms.ListBox();
            this.lblCamp2 = new System.Windows.Forms.Label();
            this.lblCamp1 = new System.Windows.Forms.Label();
            this.lblTicketIDRSV = new System.Windows.Forms.Label();
            this.tbTicketID2 = new System.Windows.Forms.TextBox();
            this.lblAvailablespotsVAL = new System.Windows.Forms.Label();
            this.lblAvailableSpots = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnShowAll = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblCamp16 = new System.Windows.Forms.Label();
            this.lbCamp16 = new System.Windows.Forms.ListBox();
            this.panelCheckInOut.SuspendLayout();
            this.panelReserve.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // rdbCollapseCheckInOut
            // 
            this.rdbCollapseCheckInOut.AutoSize = true;
            this.rdbCollapseCheckInOut.Location = new System.Drawing.Point(4, 26);
            this.rdbCollapseCheckInOut.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rdbCollapseCheckInOut.Name = "rdbCollapseCheckInOut";
            this.rdbCollapseCheckInOut.Size = new System.Drawing.Size(122, 24);
            this.rdbCollapseCheckInOut.TabIndex = 0;
            this.rdbCollapseCheckInOut.TabStop = true;
            this.rdbCollapseCheckInOut.Text = "Check in/out";
            this.rdbCollapseCheckInOut.UseVisualStyleBackColor = true;
            this.rdbCollapseCheckInOut.CheckedChanged += new System.EventHandler(this.rbCollapseCheckInOut_CheckedChanged);
            // 
            // rdbCreateReservation
            // 
            this.rdbCreateReservation.AutoSize = true;
            this.rdbCreateReservation.Location = new System.Drawing.Point(0, 75);
            this.rdbCreateReservation.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rdbCreateReservation.Name = "rdbCreateReservation";
            this.rdbCreateReservation.Size = new System.Drawing.Size(119, 24);
            this.rdbCreateReservation.TabIndex = 1;
            this.rdbCreateReservation.TabStop = true;
            this.rdbCreateReservation.Text = "Reservation";
            this.rdbCreateReservation.UseVisualStyleBackColor = true;
            this.rdbCreateReservation.CheckedChanged += new System.EventHandler(this.rdbCreateReservation_CheckedChanged);
            // 
            // lblTicketIDCO
            // 
            this.lblTicketIDCO.AutoSize = true;
            this.lblTicketIDCO.Location = new System.Drawing.Point(91, 31);
            this.lblTicketIDCO.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTicketIDCO.Name = "lblTicketIDCO";
            this.lblTicketIDCO.Size = new System.Drawing.Size(72, 20);
            this.lblTicketIDCO.TabIndex = 3;
            this.lblTicketIDCO.Text = "TicketID:";
            // 
            // btnCheckIn
            // 
            this.btnCheckIn.Location = new System.Drawing.Point(294, 103);
            this.btnCheckIn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCheckIn.Name = "btnCheckIn";
            this.btnCheckIn.Size = new System.Drawing.Size(118, 35);
            this.btnCheckIn.TabIndex = 4;
            this.btnCheckIn.Text = "Check in";
            this.btnCheckIn.UseVisualStyleBackColor = true;
            this.btnCheckIn.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbTicketID
            // 
            this.tbTicketID.Location = new System.Drawing.Point(199, 26);
            this.tbTicketID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbTicketID.Name = "tbTicketID";
            this.tbTicketID.Size = new System.Drawing.Size(276, 26);
            this.tbTicketID.TabIndex = 5;
            // 
            // panelCheckInOut
            // 
            this.panelCheckInOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panelCheckInOut.Controls.Add(this.btnShowAll);
            this.panelCheckInOut.Controls.Add(this.btnSearch);
            this.panelCheckInOut.Controls.Add(this.tbSearch);
            this.panelCheckInOut.Controls.Add(this.panelReserve);
            this.panelCheckInOut.Controls.Add(this.tbTicketID);
            this.panelCheckInOut.Controls.Add(this.lblTicketIDCO);
            this.panelCheckInOut.Controls.Add(this.btnCheckIn);
            this.panelCheckInOut.Location = new System.Drawing.Point(1465, 936);
            this.panelCheckInOut.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panelCheckInOut.Name = "panelCheckInOut";
            this.panelCheckInOut.Size = new System.Drawing.Size(633, 618);
            this.panelCheckInOut.TabIndex = 7;
            // 
            // btnCheckOut
            // 
            this.btnCheckOut.Location = new System.Drawing.Point(348, 131);
            this.btnCheckOut.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCheckOut.Name = "btnCheckOut";
            this.btnCheckOut.Size = new System.Drawing.Size(118, 35);
            this.btnCheckOut.TabIndex = 10;
            this.btnCheckOut.Text = "Check out";
            this.btnCheckOut.UseVisualStyleBackColor = true;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 20;
            this.listBox2.Location = new System.Drawing.Point(64, 243);
            this.listBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(529, 344);
            this.listBox2.TabIndex = 9;
            // 
            // panelReserve
            // 
            this.panelReserve.BackColor = System.Drawing.SystemColors.Control;
            this.panelReserve.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelReserve.BackgroundImage")));
            this.panelReserve.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelReserve.Controls.Add(this.panel6);
            this.panelReserve.Controls.Add(this.panel5);
            this.panelReserve.Controls.Add(this.panel8);
            this.panelReserve.Controls.Add(this.panel3);
            this.panelReserve.Controls.Add(this.panel2);
            this.panelReserve.Controls.Add(this.btnAdd);
            this.panelReserve.Controls.Add(this.tbCampingSpotID);
            this.panelReserve.Controls.Add(this.lblCampingspotnr);
            this.panelReserve.Controls.Add(this.panelCheckInOut);
            this.panelReserve.Controls.Add(this.lblTicketIDRSV);
            this.panelReserve.Controls.Add(this.tbTicketID2);
            this.panelReserve.Controls.Add(this.lblAvailablespotsVAL);
            this.panelReserve.Controls.Add(this.lblAvailableSpots);
            this.panelReserve.ForeColor = System.Drawing.Color.Black;
            this.panelReserve.Location = new System.Drawing.Point(944, 147);
            this.panelReserve.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panelReserve.Name = "panelReserve";
            this.panelReserve.Size = new System.Drawing.Size(1587, 1034);
            this.panelReserve.TabIndex = 8;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(241, 987);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(112, 35);
            this.btnAdd.TabIndex = 53;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // tbCampingSpotID
            // 
            this.tbCampingSpotID.Location = new System.Drawing.Point(178, 809);
            this.tbCampingSpotID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbCampingSpotID.Name = "tbCampingSpotID";
            this.tbCampingSpotID.Size = new System.Drawing.Size(247, 26);
            this.tbCampingSpotID.TabIndex = 52;
            // 
            // lblCampingspotnr
            // 
            this.lblCampingspotnr.AutoSize = true;
            this.lblCampingspotnr.Location = new System.Drawing.Point(22, 814);
            this.lblCampingspotnr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCampingspotnr.Name = "lblCampingspotnr";
            this.lblCampingspotnr.Size = new System.Drawing.Size(132, 20);
            this.lblCampingspotnr.TabIndex = 51;
            this.lblCampingspotnr.Text = "Camping spot ID:";
            // 
            // lblCamp15
            // 
            this.lblCamp15.AutoSize = true;
            this.lblCamp15.Location = new System.Drawing.Point(14, 132);
            this.lblCamp15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp15.Name = "lblCamp15";
            this.lblCamp15.Size = new System.Drawing.Size(0, 20);
            this.lblCamp15.TabIndex = 50;
            // 
            // lblCamp14
            // 
            this.lblCamp14.AutoSize = true;
            this.lblCamp14.Location = new System.Drawing.Point(13, 10);
            this.lblCamp14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp14.Name = "lblCamp14";
            this.lblCamp14.Size = new System.Drawing.Size(0, 20);
            this.lblCamp14.TabIndex = 49;
            // 
            // lblCamp13
            // 
            this.lblCamp13.AutoSize = true;
            this.lblCamp13.Location = new System.Drawing.Point(15, 251);
            this.lblCamp13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp13.Name = "lblCamp13";
            this.lblCamp13.Size = new System.Drawing.Size(0, 20);
            this.lblCamp13.TabIndex = 48;
            // 
            // lblCamp10
            // 
            this.lblCamp10.AutoSize = true;
            this.lblCamp10.Location = new System.Drawing.Point(10, 497);
            this.lblCamp10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp10.Name = "lblCamp10";
            this.lblCamp10.Size = new System.Drawing.Size(0, 20);
            this.lblCamp10.TabIndex = 47;
            // 
            // lbCamp12
            // 
            this.lbCamp12.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp12.FormattingEnabled = true;
            this.lbCamp12.ItemHeight = 20;
            this.lbCamp12.Location = new System.Drawing.Point(9, 157);
            this.lbCamp12.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp12.Name = "lbCamp12";
            this.lbCamp12.Size = new System.Drawing.Size(223, 84);
            this.lbCamp12.TabIndex = 46;
            // 
            // lblCamp12
            // 
            this.lblCamp12.AutoSize = true;
            this.lblCamp12.Location = new System.Drawing.Point(15, 131);
            this.lblCamp12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp12.Name = "lblCamp12";
            this.lblCamp12.Size = new System.Drawing.Size(0, 20);
            this.lblCamp12.TabIndex = 45;
            // 
            // lbCamp11
            // 
            this.lbCamp11.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp11.FormattingEnabled = true;
            this.lbCamp11.ItemHeight = 20;
            this.lbCamp11.Location = new System.Drawing.Point(9, 34);
            this.lbCamp11.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp11.Name = "lbCamp11";
            this.lbCamp11.Size = new System.Drawing.Size(223, 84);
            this.lbCamp11.TabIndex = 44;
            // 
            // lblCamp11
            // 
            this.lblCamp11.AutoSize = true;
            this.lblCamp11.Location = new System.Drawing.Point(16, 9);
            this.lblCamp11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp11.Name = "lblCamp11";
            this.lblCamp11.Size = new System.Drawing.Size(0, 20);
            this.lblCamp11.TabIndex = 43;
            // 
            // lbCamp10
            // 
            this.lbCamp10.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp10.FormattingEnabled = true;
            this.lbCamp10.ItemHeight = 20;
            this.lbCamp10.Location = new System.Drawing.Point(4, 521);
            this.lbCamp10.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp10.Name = "lbCamp10";
            this.lbCamp10.Size = new System.Drawing.Size(233, 84);
            this.lbCamp10.TabIndex = 42;
            // 
            // lbCamp15
            // 
            this.lbCamp15.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp15.FormattingEnabled = true;
            this.lbCamp15.ItemHeight = 20;
            this.lbCamp15.Location = new System.Drawing.Point(7, 157);
            this.lbCamp15.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp15.Name = "lbCamp15";
            this.lbCamp15.Size = new System.Drawing.Size(223, 84);
            this.lbCamp15.TabIndex = 41;
            // 
            // lbCamp14
            // 
            this.lbCamp14.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp14.FormattingEnabled = true;
            this.lbCamp14.ItemHeight = 20;
            this.lbCamp14.Location = new System.Drawing.Point(7, 35);
            this.lbCamp14.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp14.Name = "lbCamp14";
            this.lbCamp14.Size = new System.Drawing.Size(223, 84);
            this.lbCamp14.TabIndex = 40;
            // 
            // lbCamp13
            // 
            this.lbCamp13.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp13.FormattingEnabled = true;
            this.lbCamp13.ItemHeight = 20;
            this.lbCamp13.Location = new System.Drawing.Point(9, 276);
            this.lbCamp13.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp13.Name = "lbCamp13";
            this.lbCamp13.Size = new System.Drawing.Size(223, 84);
            this.lbCamp13.TabIndex = 39;
            // 
            // lbCamp9
            // 
            this.lbCamp9.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp9.FormattingEnabled = true;
            this.lbCamp9.ItemHeight = 20;
            this.lbCamp9.Location = new System.Drawing.Point(4, 402);
            this.lbCamp9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp9.Name = "lbCamp9";
            this.lbCamp9.Size = new System.Drawing.Size(233, 84);
            this.lbCamp9.TabIndex = 38;
            // 
            // lblCamp9
            // 
            this.lblCamp9.AutoSize = true;
            this.lblCamp9.Location = new System.Drawing.Point(10, 378);
            this.lblCamp9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp9.Name = "lblCamp9";
            this.lblCamp9.Size = new System.Drawing.Size(0, 20);
            this.lblCamp9.TabIndex = 37;
            // 
            // lbCamp8
            // 
            this.lbCamp8.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp8.FormattingEnabled = true;
            this.lbCamp8.ItemHeight = 20;
            this.lbCamp8.Location = new System.Drawing.Point(4, 287);
            this.lbCamp8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp8.Name = "lbCamp8";
            this.lbCamp8.Size = new System.Drawing.Size(233, 84);
            this.lbCamp8.TabIndex = 36;
            // 
            // lblCamp8
            // 
            this.lblCamp8.AutoSize = true;
            this.lblCamp8.Location = new System.Drawing.Point(11, 262);
            this.lblCamp8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp8.Name = "lblCamp8";
            this.lblCamp8.Size = new System.Drawing.Size(0, 20);
            this.lblCamp8.TabIndex = 35;
            // 
            // lbCamp7
            // 
            this.lbCamp7.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp7.FormattingEnabled = true;
            this.lbCamp7.ItemHeight = 20;
            this.lbCamp7.Location = new System.Drawing.Point(4, 165);
            this.lbCamp7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp7.Name = "lbCamp7";
            this.lbCamp7.Size = new System.Drawing.Size(233, 84);
            this.lbCamp7.TabIndex = 34;
            // 
            // lblCamp7
            // 
            this.lblCamp7.AutoSize = true;
            this.lblCamp7.Location = new System.Drawing.Point(11, 140);
            this.lblCamp7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp7.Name = "lblCamp7";
            this.lblCamp7.Size = new System.Drawing.Size(0, 20);
            this.lblCamp7.TabIndex = 33;
            // 
            // lbCamp6
            // 
            this.lbCamp6.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp6.FormattingEnabled = true;
            this.lbCamp6.ItemHeight = 20;
            this.lbCamp6.Location = new System.Drawing.Point(4, 35);
            this.lbCamp6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp6.Name = "lbCamp6";
            this.lbCamp6.Size = new System.Drawing.Size(233, 84);
            this.lbCamp6.TabIndex = 32;
            // 
            // lblCamp6
            // 
            this.lblCamp6.AutoSize = true;
            this.lblCamp6.Location = new System.Drawing.Point(10, 10);
            this.lblCamp6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp6.Name = "lblCamp6";
            this.lblCamp6.Size = new System.Drawing.Size(0, 20);
            this.lblCamp6.TabIndex = 31;
            // 
            // lbCamp5
            // 
            this.lbCamp5.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp5.FormattingEnabled = true;
            this.lbCamp5.ItemHeight = 20;
            this.lbCamp5.Location = new System.Drawing.Point(4, 521);
            this.lbCamp5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp5.Name = "lbCamp5";
            this.lbCamp5.Size = new System.Drawing.Size(233, 84);
            this.lbCamp5.TabIndex = 30;
            // 
            // lblCamp5
            // 
            this.lblCamp5.AutoSize = true;
            this.lblCamp5.Location = new System.Drawing.Point(10, 496);
            this.lblCamp5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp5.Name = "lblCamp5";
            this.lblCamp5.Size = new System.Drawing.Size(0, 20);
            this.lblCamp5.TabIndex = 29;
            // 
            // lbCamp4
            // 
            this.lbCamp4.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp4.FormattingEnabled = true;
            this.lbCamp4.ItemHeight = 20;
            this.lbCamp4.Location = new System.Drawing.Point(4, 402);
            this.lbCamp4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp4.Name = "lbCamp4";
            this.lbCamp4.Size = new System.Drawing.Size(233, 84);
            this.lbCamp4.TabIndex = 28;
            // 
            // lblCamp4
            // 
            this.lblCamp4.AutoSize = true;
            this.lblCamp4.Location = new System.Drawing.Point(10, 378);
            this.lblCamp4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp4.Name = "lblCamp4";
            this.lblCamp4.Size = new System.Drawing.Size(0, 20);
            this.lblCamp4.TabIndex = 27;
            // 
            // lbCamp3
            // 
            this.lbCamp3.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp3.FormattingEnabled = true;
            this.lbCamp3.ItemHeight = 20;
            this.lbCamp3.Location = new System.Drawing.Point(4, 287);
            this.lbCamp3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp3.Name = "lbCamp3";
            this.lbCamp3.Size = new System.Drawing.Size(233, 84);
            this.lbCamp3.TabIndex = 26;
            // 
            // lblCamp3
            // 
            this.lblCamp3.AutoSize = true;
            this.lblCamp3.Location = new System.Drawing.Point(11, 262);
            this.lblCamp3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp3.Name = "lblCamp3";
            this.lblCamp3.Size = new System.Drawing.Size(0, 20);
            this.lblCamp3.TabIndex = 25;
            // 
            // lbCamp2
            // 
            this.lbCamp2.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp2.FormattingEnabled = true;
            this.lbCamp2.ItemHeight = 20;
            this.lbCamp2.Location = new System.Drawing.Point(4, 165);
            this.lbCamp2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp2.Name = "lbCamp2";
            this.lbCamp2.Size = new System.Drawing.Size(233, 84);
            this.lbCamp2.TabIndex = 24;
            // 
            // lbCamp1
            // 
            this.lbCamp1.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp1.FormattingEnabled = true;
            this.lbCamp1.ItemHeight = 20;
            this.lbCamp1.Location = new System.Drawing.Point(4, 35);
            this.lbCamp1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp1.Name = "lbCamp1";
            this.lbCamp1.Size = new System.Drawing.Size(233, 84);
            this.lbCamp1.TabIndex = 23;
            // 
            // lblCamp2
            // 
            this.lblCamp2.AutoSize = true;
            this.lblCamp2.Location = new System.Drawing.Point(14, 140);
            this.lblCamp2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp2.Name = "lblCamp2";
            this.lblCamp2.Size = new System.Drawing.Size(0, 20);
            this.lblCamp2.TabIndex = 8;
            // 
            // lblCamp1
            // 
            this.lblCamp1.AutoSize = true;
            this.lblCamp1.Location = new System.Drawing.Point(14, 10);
            this.lblCamp1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamp1.Name = "lblCamp1";
            this.lblCamp1.Size = new System.Drawing.Size(0, 20);
            this.lblCamp1.TabIndex = 7;
            // 
            // lblTicketIDRSV
            // 
            this.lblTicketIDRSV.AutoSize = true;
            this.lblTicketIDRSV.Location = new System.Drawing.Point(22, 865);
            this.lblTicketIDRSV.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTicketIDRSV.Name = "lblTicketIDRSV";
            this.lblTicketIDRSV.Size = new System.Drawing.Size(68, 20);
            this.lblTicketIDRSV.TabIndex = 5;
            this.lblTicketIDRSV.Text = "TicketID";
            // 
            // tbTicketID2
            // 
            this.tbTicketID2.Location = new System.Drawing.Point(178, 860);
            this.tbTicketID2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbTicketID2.Name = "tbTicketID2";
            this.tbTicketID2.Size = new System.Drawing.Size(247, 26);
            this.tbTicketID2.TabIndex = 4;
            // 
            // lblAvailablespotsVAL
            // 
            this.lblAvailablespotsVAL.AutoSize = true;
            this.lblAvailablespotsVAL.Location = new System.Drawing.Point(208, 15);
            this.lblAvailablespotsVAL.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAvailablespotsVAL.Name = "lblAvailablespotsVAL";
            this.lblAvailablespotsVAL.Size = new System.Drawing.Size(0, 20);
            this.lblAvailablespotsVAL.TabIndex = 2;
            // 
            // lblAvailableSpots
            // 
            this.lblAvailableSpots.AutoSize = true;
            this.lblAvailableSpots.Location = new System.Drawing.Point(57, 4);
            this.lblAvailableSpots.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAvailableSpots.Name = "lblAvailableSpots";
            this.lblAvailableSpots.Size = new System.Drawing.Size(130, 20);
            this.lblAvailableSpots.TabIndex = 1;
            this.lblAvailableSpots.Text = "Camping ground:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel1.Controls.Add(this.rdbCreateReservation);
            this.panel1.Controls.Add(this.rdbCollapseCheckInOut);
            this.panel1.Location = new System.Drawing.Point(4, 18);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(147, 1030);
            this.panel1.TabIndex = 9;
            // 
            // tbSearch
            // 
            this.panel2.BackColor = System.Drawing.Color.SeaShell;
            this.panel2.Controls.Add(this.lbCamp1);
            this.panel2.Controls.Add(this.lbCamp2);
            this.panel2.Controls.Add(this.lbCamp3);
            this.panel2.Controls.Add(this.lbCamp4);
            this.panel2.Controls.Add(this.lbCamp5);
            this.panel2.Controls.Add(this.lblCamp1);
            this.panel2.Controls.Add(this.lblCamp2);
            this.panel2.Controls.Add(this.lblCamp5);
            this.panel2.Controls.Add(this.lblCamp3);
            this.panel2.Controls.Add(this.lblCamp4);
            this.panel2.Location = new System.Drawing.Point(26, 30);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(241, 619);
            this.panel2.TabIndex = 54;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SeaShell;
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.lbCamp6);
            this.panel3.Controls.Add(this.lbCamp7);
            this.panel3.Controls.Add(this.lbCamp8);
            this.panel3.Controls.Add(this.lbCamp9);
            this.panel3.Controls.Add(this.lbCamp10);
            this.panel3.Controls.Add(this.lblCamp6);
            this.panel3.Controls.Add(this.lblCamp7);
            this.panel3.Controls.Add(this.lblCamp8);
            this.panel3.Controls.Add(this.lblCamp9);
            this.panel3.Controls.Add(this.lblCamp10);
            this.panel3.Location = new System.Drawing.Point(286, 30);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(241, 619);
            this.panel3.TabIndex = 55;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 20);
            this.label1.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 140);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 20);
            this.label2.TabIndex = 8;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.SeaShell;
            this.panel8.Controls.Add(this.lbCamp11);
            this.panel8.Controls.Add(this.lbCamp12);
            this.panel8.Controls.Add(this.lbCamp13);
            this.panel8.Controls.Add(this.lblCamp11);
            this.panel8.Controls.Add(this.lblCamp12);
            this.panel8.Controls.Add(this.lblCamp13);
            this.panel8.Location = new System.Drawing.Point(1064, 505);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(241, 430);
            this.panel8.TabIndex = 56;
            // 
            // panel5
            // 
            this.panel5.Location = new System.Drawing.Point(-192, -81);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(200, 100);
            this.panel5.TabIndex = 57;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.SeaShell;
            this.panel6.Controls.Add(this.lbCamp16);
            this.panel6.Controls.Add(this.lblCamp16);
            this.panel6.Controls.Add(this.lbCamp14);
            this.panel6.Controls.Add(this.lbCamp15);
            this.panel6.Controls.Add(this.lblCamp14);
            this.panel6.Controls.Add(this.lblCamp15);
            this.panel6.Location = new System.Drawing.Point(1311, 505);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(238, 430);
            this.panel6.TabIndex = 57;
            // 
            // lblCamp16
            // 
            this.lblCamp16.AutoSize = true;
            this.lblCamp16.Location = new System.Drawing.Point(11, 246);
            this.lblCamp16.Name = "lblCamp16";
            this.lblCamp16.Size = new System.Drawing.Size(0, 20);
            this.lblCamp16.TabIndex = 42;
            // 
            // lbCamp16
            // 
            this.lbCamp16.BackColor = System.Drawing.Color.SeaShell;
            this.lbCamp16.FormattingEnabled = true;
            this.lbCamp16.ItemHeight = 20;
            this.lbCamp16.Location = new System.Drawing.Point(7, 268);
            this.lbCamp16.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbCamp16.Name = "lbCamp16";
            this.lbCamp16.Size = new System.Drawing.Size(223, 84);
            this.lbCamp16.TabIndex = 43;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1764, 1050);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Camping spot Management";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panelCheckInOut.ResumeLayout(false);
            this.panelCheckInOut.PerformLayout();
            this.panelReserve.ResumeLayout(false);
            this.panelReserve.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton rdbCollapseCheckInOut;
        private System.Windows.Forms.RadioButton rdbCreateReservation;
        private System.Windows.Forms.Label lblTicketIDCO;
        private System.Windows.Forms.Button btnCheckIn;
        private System.Windows.Forms.TextBox tbTicketID;
        private System.Windows.Forms.Panel panelCheckInOut;
        private System.Windows.Forms.Panel panelReserve;
        private System.Windows.Forms.Label lblAvailablespotsVAL;
        private System.Windows.Forms.Label lblAvailableSpots;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label lblTicketIDRSV;
        private System.Windows.Forms.TextBox tbTicketID2;
        private System.Windows.Forms.Button btnCheckOut;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox tbCampingSpotID;
        private System.Windows.Forms.Label lblCampingspotnr;
        private System.Windows.Forms.Label lblCamp15;
        private System.Windows.Forms.Label lblCamp14;
        private System.Windows.Forms.Label lblCamp13;
        private System.Windows.Forms.Label lblCamp10;
        private System.Windows.Forms.ListBox lbCamp12;
        private System.Windows.Forms.Label lblCamp12;
        private System.Windows.Forms.ListBox lbCamp11;
        private System.Windows.Forms.Label lblCamp11;
        private System.Windows.Forms.ListBox lbCamp10;
        private System.Windows.Forms.ListBox lbCamp15;
        private System.Windows.Forms.ListBox lbCamp14;
        private System.Windows.Forms.ListBox lbCamp13;
        private System.Windows.Forms.ListBox lbCamp9;
        private System.Windows.Forms.Label lblCamp9;
        private System.Windows.Forms.ListBox lbCamp8;
        private System.Windows.Forms.Label lblCamp8;
        private System.Windows.Forms.ListBox lbCamp7;
        private System.Windows.Forms.Label lblCamp7;
        private System.Windows.Forms.ListBox lbCamp6;
        private System.Windows.Forms.Label lblCamp6;
        private System.Windows.Forms.ListBox lbCamp5;
        private System.Windows.Forms.Label lblCamp5;
        private System.Windows.Forms.ListBox lbCamp4;
        private System.Windows.Forms.Label lblCamp4;
        private System.Windows.Forms.ListBox lbCamp3;
        private System.Windows.Forms.Label lblCamp3;
        private System.Windows.Forms.ListBox lbCamp2;
        private System.Windows.Forms.ListBox lbCamp1;
        private System.Windows.Forms.Label lblCamp2;
        private System.Windows.Forms.Label lblCamp1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lbCamp16;
        private System.Windows.Forms.Label lblCamp16;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Button btnShowAll;
    }
}

