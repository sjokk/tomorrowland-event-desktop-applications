﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Phidget22;
using Phidget22.Events;
using MySql.Data.MySqlClient;
using MySql.Data;

namespace generalGUI
{
    public partial class Form1 : Form
    {
        CampingSpotManagement csm = new CampingSpotManagement();
        private RFID RFIDReader;



        public Form1()
        {
            InitializeComponent();
            rdbCollapseCheckInOut.Checked = true;
            changeFormGUI();
            //first retrieve the data from the db
            csm.RetrieveAllCampingSpotsfromdb();
            //helper method to populate the textboxes
            FillAllTextBoxes();
            RFIDReader = new RFID();
            RFIDReader.Open();
            try
            {
                RFIDReader.Tag += RFIDReader_Tag;
            }
            catch(PhidgetException ex)
            {
                MessageBox.Show(ex.Message);
            }

           
        }

        private void RFIDReader_Tag(object sender, RFIDTagEventArgs e)
        {
            tbTicketID.Clear();
            tbTicketID2.Clear();
            tbTicketID.Text = csm.RetrieveTicketIDFromRFID(e.Tag);
            tbTicketID2.Text = csm.RetrieveTicketIDFromRFID(e.Tag);
        }
        private void FillAllTextBoxes()
        {
            ListBox[] Boxes = { lbCamp1, lbCamp2, lbCamp3, lbCamp4, lbCamp5, lbCamp6, lbCamp7, lbCamp8, lbCamp9, lbCamp10, lbCamp11, lbCamp12, lbCamp13, lbCamp14, lbCamp15, lbCamp16 };
            for (int i = 0; i < Boxes.Length; i++)
            {
                Boxes[i].Items.Clear();
            }
            Label[] labelsArray = { lblCamp1, lblCamp2, lblCamp3, lblCamp4, lblCamp5, lblCamp6, lblCamp7, lblCamp8, lblCamp9, lblCamp10, lblCamp11, lblCamp12, lblCamp13, lblCamp14, lblCamp15, lblCamp16 };

            for (int i = 0; i < csm.ReturnCampingSpots().Count; i++)
            {

                labelsArray[i].Text = csm.ReturnCampingSpots()[i].ToString();
                CampingSpot c = csm.GetCampingSpot(csm.ReturnCampingSpots()[i].ID);

                foreach (Participant p in c.GetAllParticipants())
                {

                    Boxes[i].Items.Add(p);
                }
                
            }


        }

        private void changeFormGUI()
        {
            if (rdbCreateReservation.Checked)
            {
                panelReserve.Visible = true;
                panelCheckInOut.Visible = false;
                
            }
            if(rdbCollapseCheckInOut.Checked)
            {

                panelCheckInOut.Visible = true;
                panelReserve.Visible = false;
               
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
          if(lbAllCheckIns.SelectedIndex != -1)
          {
                CheckInOut c = (CheckInOut)lbAllCheckIns.SelectedItem;
                c.isCheckedIn = 0;
                PopulateCheckInOutBoxes();
          }
          

            
        }

        private void rbCollapseCheckInOut_CheckedChanged(object sender, EventArgs e)
        {
            changeFormGUI();
        }

        private void rdbCreateReservation_CheckedChanged(object sender, EventArgs e)
        {
            changeFormGUI();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
         
        }

        private void btnReserveSpot_Click(object sender, EventArgs e)
        {
            
           
            
            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int campingspotid = Convert.ToInt32(tbCampingSpotID.Text);
                int ticketid = Convert.ToInt32(tbTicketID2.Text);


                csm.HandleParticipantsOnAdd(ticketid, campingspotid);

            }
            catch (ParticipantException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                FillAllTextBoxes();
            }

            
            


        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            lbAllCheckIns.Items.Clear();
            lbAllCheckOuts.Items.Clear();
            if (tbSearch.Text != "")
            {
                CheckInOut c = csm.findPerson(tbSearch.Text);
                if(c != null)
                {
                    if (c.isCheckedIn == 0)
                    {
                        string info = "Checked out";
                        lbAllCheckOuts.Items.Add($"Checked in: {c}");
                    }
                    else
                    {
                        lbAllCheckIns.Items.Add($"{c}");
                    }
                }
               

            }
                
            
        }
        private void btnShowAll_Click(object sender, EventArgs e)
        {
            lbAllCheckIns.Items.Clear();
            lbAllCheckOuts.Items.Clear();
            PopulateCheckInOutBoxes();
        }
    }
}
