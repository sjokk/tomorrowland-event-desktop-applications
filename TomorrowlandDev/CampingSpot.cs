﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBaseMethods;
using MySql.Data;
using MySql.Data.MySqlClient;


namespace generalGUI
{
    class CampingSpot : DataBase
    {
        public int ID
        { get; set; }

        public List<Participant> participants;

        public string Campingspotname
        {
            get; set;
        }

        

      

        public CampingSpot(int id,string Name)
        {
            this.ID = id;
            this.Campingspotname = Name;
            
            participants = new List<Participant>();
            
        }

        //TBH
        //public bool AddParticipant(string firstname, string lastname)
        //{

        //    //First check if this participant is already in the camping spot
        //    if (GetParticipant(firstname, lastname) == null)
        //    {
        //        //if he is not found as a participant in the camping spot, check if the camping is full
        //        if (this.participants.Count < 6)
        //        {
        //            this.participants.Add(new Participant(firstname, lastname));
        //            string sql = ($"UPDATE `ticket` SET `idCampingspot`= {this.ID} WHERE idTicket = (SELECT t.idTicket from ticket t join customer c on(t.idCustomer = c.idCustomer) WHERE LOWER(c.First_Name) = '{firstname}' and LOWER(c.Last_Name) = '{lastname}';");
        //            ExecuteNonQuery(sql);
        //            return true;
        //        }
        //        //in this case, the camping spots had more or equal to 6, so we throw an exception letting them know it's full

        //        throw new ParticipantException("User cannot be added because this camping spot is full");

        //    }
        //    else if (GetParticipant(firstname, lastname) != null)
        //    {
        //        throw new ParticipantException("This user is already in this camping spot");
        //    }

        //    return false;
        //}

        public bool AddParticipant(int ticketid)
        {
            if (this.participants.Count < 6)
            {
                
                string sql = ($"select t.idTicket, c.First_Name,c.Last_Name from customer c join ticket t on (c.idCustomer = t.idCustomer) where t.Idticket = {ticketid} and t.idEvent = 2 ");
                int pticketid = 0;
                string pfirstname = null;
                string plastname = null;
                string sql2 = ($"select t.idTicket, c.First_Name,c.Last_Name from customer c join ticket t on (c.idCustomer = t.idCustomer) join campingspot cs on (t.idCampingspot = cs.idCampingspot) where t.Idticket = {ticketid} and t.idEvent = 1 ");

                MySqlDataReader mdr = ExecuteReader(sql);
                while(mdr.Read())
                {
                    pticketid = Convert.ToInt32(mdr[0]);
                    pfirstname = Convert.ToString(mdr[1]);
                    plastname = Convert.ToString(mdr[2]);
                }
                Participant p = new Participant(pticketid, pfirstname, plastname);
                this.participants.Add(p);
                CloseConnection();
                return true;

            }
            return false;

        }

        public List<Participant> GetAllParticipants()
        {
            return this.participants;
        }

        public Participant GetParticipant(string firstname , string lastname)
        {
            foreach (Participant p in this.participants)
            {
                if(p.FirstName == firstname && p.LastName == lastname)
                {
                    return p;
                }
            }
            return null;
        }

        public Participant GetParticipant(int id)
        {
            foreach (Participant p in this.GetAllParticipants())
            {
                if(p.IDTicket == id)
                {
                    return p;
                }
            }
            return null;
        }

        public void RetrieveCSpotData(string name)
        {
            String connectionstring = "server=studmysql01.fhict.local;database=dbi231896;uid=dbi231896;password=UnsafePassword;connect timeout=30;";

                using (MySqlConnection connection = new MySqlConnection(connectionstring))
                {
                
                    string sql = ($"select t.idTicket, c.First_Name,c.Last_Name from customer c join ticket t on (c.idCustomer = t.idCustomer) join campingspot cs on (t.idCampingspot = cs.idCampingspot) where cs.Name = '{this.Campingspotname}' and t.idEvent = 2 ");
                    MySqlCommand cmd = new MySqlCommand(sql, connection);
                    MySqlDataReader mdr = cmd.ExecuteReader();
                    while (mdr.Read())
                    {
                        int ticketid = Convert.ToInt32(mdr[0]);
                        string firstname = Convert.ToString(mdr[1]);
                        string lastname = Convert.ToString(mdr[2]);

                        participants.Add(new Participant(ticketid, firstname, lastname));
                    }



                }



            


            CloseConnection();

        }

        public override string ToString()
        {
            return ($"{this.ID}. {this.Campingspotname}");
        }

    }
}
